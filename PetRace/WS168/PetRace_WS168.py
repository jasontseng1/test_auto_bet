# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from Setting import test_data
import time
from Lib import get_report

game_list_dict = ["鬥雞",]


class PetRaceWS168AutoBet:
    """ WS168動競自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        result_list = []

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='動競']]"))).click()

        # WS168
        try:
            print("測試項目：「WS168」自動下注")
            # 點擊WS168
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='row_animal']//a[@class='lnk_enterGame']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            # 判斷是否進入遊戲
            ele = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                (By.XPATH, "//p[contains(@class, 'bold hover-user-text')]")))
            if ele.get_attribute('innerHTML') != test_data['account'][0]:
                print("Fail: 顯示帳號與登入帳號不同")
                print(f"頁面帳號:{ele.get_attribute('innerHTML')}")
                print(f"登入帳號:{test_data['account'][0]}")
                return False

            # 判斷是否能下注
            WebDriverWait(self.driver, 180).until(EC.presence_of_element_located(
                (By.XPATH, "//span[text()='最後投注' or text()='開始下注']")))

            # 下注
            ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//p/span[text()='龍']")))
            self.driver.execute_script("arguments[0].click();", ele)
            time.sleep(1)

            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div/span[text()='下注']"))).click()
            time.sleep(1)

            # 點擊 確認視窗
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='確定']"))).click()

            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//p[text()='下注成功']")))

            # 判斷下注金額
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[./p[text()='總下注額']]")))

            if int(ele.text.split("\n")[-1]) == 0:
                result_list.append("False")
            else:
                print("***自動下注成功***")

            self.get_img("WS168")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail:「WS168」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class PetRaceWS168AutoReport:
    """ WS168動競自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('7', 'WS168')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'WS168')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        # 登入代理端
        get_manage_report.login()
        # 取得後台報表資料
        manager_report, page_data, page_check_result = get_manage_report.search_report('7', 'WS168', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'WS168')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 登入超帳
        get_manage_report.super_login()
        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('7', 'WS168', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'WS168', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], game_list_dict, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], game_list_dict, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
