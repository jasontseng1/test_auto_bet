# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from RSG import Fish_RSG
from JDB import Fish_JDB
from DS import Fish_DS
from JILI import Fish_JILI
from FC import Fish_FC
from MG import Fish_MG
from JOKER import Fish_JOKER
from TP import Fish_TP
from GR import Fish_GR

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()
driver.get(test_data['member_url'])


class FishReport(unittest.TestCase):
    """ 捕魚機報表 """

    f_rsg_ar = Fish_RSG.FishRSGAutoReport(driver, get_img)
    f_jdb_ar = Fish_JDB.FishJDBAutoReport(driver, get_img)
    f_ds_ar = Fish_DS.FishDSAutoReport(driver, get_img)
    f_jili_ar = Fish_JILI.FishJILIAutoReport(driver, get_img)
    f_fc_ar = Fish_FC.FishFCAutoReport(driver, get_img)
    f_mg_ar = Fish_MG.FishMGAutoReport(driver, get_img)
    f_joker_ar = Fish_JOKER.FishJOKERAutoReport(driver, get_img)
    f_tp_ar = Fish_TP.FishTPAutoReport(driver, get_img)
    f_gr_ar = Fish_GR.FishGRAutoReport(driver, get_img)

    def test_login(self):
        """ 登入 """

        result = True

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result = False

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result = False

        self.assertEqual(True, result)

    def test_fish_rsg_report(self):
        """ RSG捕魚機報表確認 """

        result = self.f_rsg_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_jdb_report(self):
        """ JDB捕魚機報表確認 """

        result = self.f_jdb_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_ds_report(self):
        """ DS捕魚機報表確認 """

        result = self.f_ds_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_jili_report(self):
        """ JILI捕魚機報表確認 """

        result = self.f_jili_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_fc_report(self):
        """ FC捕魚機報表確認 """

        result = self.f_fc_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_mg_report(self):
        """ MG捕魚機報表確認 """

        result = self.f_mg_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_joker_report(self):
        """ JOKER捕魚機報表確認 """

        result = self.f_joker_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_tp_report(self):
        """ TP捕魚機報表確認 """

        result = self.f_tp_ar.auto_report()
        self.assertEqual(True, result)

    def test_fish_gr_report(self):
        """ GR捕魚機報表確認 """

        result = self.f_gr_ar.auto_report()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        FishReport("test_login"),
        FishReport("test_fish_rsg_report"),
        FishReport("test_fish_jdb_report"),
        FishReport("test_fish_ds_report"),
        FishReport("test_fish_jili_report"),
        FishReport("test_fish_fc_report"),
        FishReport("test_fish_mg_report"),
        FishReport("test_fish_joker_report"),
        FishReport("test_fish_tp_report"),
        FishReport("test_fish_gr_report")
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/Report/Report_' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='捕魚機自動報表',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
