# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Lib import img_recognition
from Lib import get_report

game_list_dict = {
    "蟲蟲樂園": "InsectParadise_bet.png",
    "海霸": "FishHunterHaiba_bet.png"
}


class FishJOKERAutoBet:
    """ JOKER捕魚自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        result_list = []
        ac = ActionChains(self.driver)
        window_size = self.driver.get_window_size()

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='捕魚機']]"))).click()

        title = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='content']")))
        self.driver.execute_script("arguments[0].scrollIntoView();", title)

        # 點擊JOKER
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='JOKER']"))).click()

        # 蟲蟲樂園
        try:
            print("測試項目：「蟲蟲樂園」自動下注")
            # 點擊開啟蟲蟲樂園
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='蟲蟲樂園 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            # 判斷是否進入下注區
            WebDriverWait(self.driver, 60).until(EC.invisibility_of_element_located(
                (By.XPATH, "//img[@id='splashlogo']")))
            time.sleep(1)

            # 下注(畫面正中間)
            ac.reset_actions()
            ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2)
            ac.click_and_hold().pause(1).release().perform()
            print("***自動下注成功***")

            self.get_img("蟲蟲樂園")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail: 「蟲蟲樂園」自動下注失敗")
            print(e)
            result_list.append("False")

        # 海霸
        try:
            print("測試項目：「海霸」自動下注")
            # 點擊開啟海霸
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='海霸 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            # 判斷是否進入下注區
            WebDriverWait(self.driver, 60).until(EC.invisibility_of_element_located(
                (By.XPATH, "//img[@id='splashlogo']")))
            time.sleep(1)

            # 下注(畫面正中間)
            ac.reset_actions()
            ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2)
            ac.click_and_hold().pause(1).release().perform()
            print("***自動下注成功***")

            self.get_img("海霸")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail: 「海霸」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class FishJOKERAutoReport:
    """ JOKER捕魚自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 切換分頁-官網
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[0])

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', 'JOKER')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'JOKER')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-後台
        self.driver.switch_to.window(handles[1])

        # 取得後台報表資料
        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        manager_report, page_data, page_check_result = get_manage_report.search_report('3', 'JOKER', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'JOKER')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-超帳
        self.driver.switch_to.window(handles[-1])

        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', 'JOKER', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'JOKER', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], game_list_dict, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], game_list_dict, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
