# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Lib import img_recognition
from Lib import get_report

game_list_dict = {
    "八爪天下海霸王": ["OceanEmperor_hall.png", "YourPosition.png"],
    "福娃捕魚": ["FuWaFishing_hall.png", "YourPosition.png"],
}


class FishRSGAutoBet:
    """ RSG捕魚機自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        # init
        result_list = []
        window_size = self.driver.get_window_size()
        ac = ActionChains(self.driver)

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='捕魚機']]"))).click()

        # 點擊RSG
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='RSG']"))).click()

        # 八爪天下海霸王
        try:
            print("測試項目：「八爪天下海霸王」自動下注")
            # 點擊開啟八爪天下海霸王
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='八爪天下海霸王 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            count = 0
            while count < 2:
                # 點擊廳別
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'RSG', game_list_dict["八爪天下海霸王"][0])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    count += 1
                    self.driver.refresh()
                    print("\n重新判斷\n")
                else:
                    break

            if count == 2:
                result_list.append("False")
                print("Fail：八爪天下海霸王自動下注失敗")
            else:
                # 判斷是否進入下注區
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'RSG', game_list_dict["八爪天下海霸王"][1])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    result_list.append("False")
                else:
                    # 下注(畫面正中間)
                    ac.reset_actions()
                    ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2).click().perform()

                    time.sleep(1)
                    print("***自動下注成功***")

            self.get_img("八爪天下海霸王")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail:「八爪天下海霸王」自動下注失敗")
            print(e)
            result_list.append("False")

        # 福娃捕魚
        try:
            print("測試項目：「福娃捕魚」自動下注")
            # 點擊開啟福娃捕魚
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='福娃捕魚 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            count = 0
            while count < 2:
                # 點擊廳別
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'RSG', game_list_dict["福娃捕魚"][0])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path, total_time=90)
                if not click_result:
                    count += 1
                    self.driver.refresh()
                    print("\n重新判斷\n")
                else:
                    break

            if count == 2:
                result_list.append("False")
                print("Fail：福娃捕魚自動下注失敗")
            else:
                # 判斷是否進入下注區
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'RSG', game_list_dict["福娃捕魚"][1])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path, total_time=90)
                if not click_result:
                    result_list.append("False")
                else:
                    # 下注(畫面正中間)
                    ac.reset_actions()
                    ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2).click().perform()

                    time.sleep(1)
                    print("***自動下注成功***")

            self.get_img("福娃捕魚")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail:「福娃捕魚」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class FishRSGAutoReport:
    """ RSG捕魚機自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', 'RSG')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'RSG')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        # 登入代理端
        get_manage_report.login()
        # 取得後台報表資料
        manager_report, page_data, page_check_result = get_manage_report.search_report('3', 'RSG', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'RSG')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 登入超帳
        get_manage_report.super_login()
        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', 'RSG', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'RSG', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 遊戲清單
        new_game_list = []
        for game in game_list_dict.keys():
            new_game_list.append(game)

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], new_game_list, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], new_game_list, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
