# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from RSG import Fish_RSG
from JDB import Fish_JDB
from DS import Fish_DS
from JILI import Fish_JILI
from FC import Fish_FC
from MG import Fish_MG
from JOKER import Fish_JOKER
from TP import Fish_TP
from GR import Fish_GR

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()
driver.get(test_data["member_url"])


class FishBet(unittest.TestCase):
    """ 捕魚機下注 """

    f_rsg_ab = Fish_RSG.FishRSGAutoBet(driver, get_img)
    f_jdb_ab = Fish_JDB.FishJDBAutoBet(driver, get_img)
    f_ds_ab = Fish_DS.FishDSAutoBet(driver, get_img)
    f_jili_ab = Fish_JILI.FishJILIAutoBet(driver, get_img)
    f_fc_ab = Fish_FC.FishFCAutoBet(driver, get_img)
    f_mg_ab = Fish_MG.FishMGAutoBet(driver, get_img)
    f_joker_ab = Fish_JOKER.FishJOKERAutoBet(driver, get_img)
    f_tp_ab = Fish_TP.FishTPAutoBet(driver, get_img)
    f_gr_ab = Fish_GR.FishGRAutoBet(driver, get_img)

    def test_login(self):
        """ 登入 """

        result = True

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result = False

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result = False

        self.assertEqual(True, result)

    def test_fish_rsg(self):
        """ RSG捕魚機自動下注 """

        result = self.f_rsg_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_jdb(self):
        """ JDB捕魚機自動下注 """

        result = self.f_jdb_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_ds(self):
        """ DS捕魚機自動下注 """

        result = self.f_ds_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_jili(self):
        """ JILI捕魚機自動下注 """

        result = self.f_jili_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_fc(self):
        """ FC捕魚機自動下注 """

        result = self.f_fc_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_mg(self):
        """ MG捕魚自動下注 """

        result = self.f_mg_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_joker(self):
        """ JOKER捕魚自動下注 """

        result = self.f_joker_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_tp(self):
        """ TP捕魚自動下注 """

        result = self.f_tp_ab.auto_bet()
        self.assertEqual(True, result)

    def test_fish_gr(self):
        """ GR捕魚自動下注 """

        result = self.f_gr_ab.auto_bet()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        FishBet("test_login"),
        FishBet("test_fish_rsg"),
        FishBet("test_fish_jdb"),
        FishBet("test_fish_ds"),
        FishBet("test_fish_jili"),
        FishBet("test_fish_fc"),
        FishBet("test_fish_mg"),
        FishBet("test_fish_joker"),
        FishBet("test_fish_tp"),
        FishBet("test_fish_gr")
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/Bet/Bet_' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='捕魚機自動下注',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
