# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Lib import img_recognition
from Lib import get_report

game_list_dict = {
    "極速彩金捕魚": ["JackpotFishing_close.png", "JackpotFishing_lock.png"],
    "開心捕魚": ["HappyFishing_continue.png", "HappyFishing_veteran.png", "HappyFishing_lock.png"],
}


class FishJILIAutoBet:
    """ JILI捕魚機自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        # init
        result_list = []
        window_size = self.driver.get_window_size()
        ac = ActionChains(self.driver)

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='捕魚機']]"))).click()

        title = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='content']")))
        self.driver.execute_script("arguments[0].scrollIntoView();", title)

        # 點擊JILI
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='JILI']"))).click()

        # 極速彩金捕魚
        try:
            print("測試項目：「極速彩金捕魚」自動下注")
            # 點擊開啟極速彩金捕魚
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='極速彩金捕魚 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            try:
                # 切換至iframe
                WebDriverWait(self.driver, 90).until(EC.frame_to_be_available_and_switch_to_it(
                    (By.XPATH, "//iframe")))

                # 等活動顯示
                WebDriverWait(self.driver, 90).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@id='__layout' or @class='sk-circle']")))

                # 點擊關閉
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'JILI', game_list_dict["極速彩金捕魚"][0])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path, total_time=20)
                if not click_result:
                    print("無活動頁面，可無視Fail")

            except:
                pass

            # 判斷是否進入下注區
            img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'JILI', game_list_dict["極速彩金捕魚"][1])
            img_path = img_coord.check_img()
            click_result = img_coord.get_coordinate(target=img_path, canny=False)
            if not click_result:
                result_list.append("False")
                print("Fail：極速彩金捕魚自動下注失敗")
            else:
                # 下注(畫面正中間)
                ac.reset_actions()
                ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2)
                ac.click_and_hold().pause(1).release().perform()

                time.sleep(1)
                print("***自動下注成功***")

            self.get_img("極速彩金捕魚")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail:「極速彩金捕魚」自動下注失敗")
            print(e)
            result_list.append("False")

        # 開心捕魚
        try:
            print("測試項目：「開心捕魚」自動下注")
            # 點擊開啟開心捕魚
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='開心捕魚 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            # 等logo消失
            WebDriverWait(self.driver, 60).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@id='LogoPage' and contains(@style, 'display: none;')]")))
            time.sleep(1)

            # 點擊繼續按鈕
            img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'JILI', game_list_dict["開心捕魚"][0])
            img_path = img_coord.check_img()
            click_result = img_coord.click_coordinate(img_path=img_path)
            if not click_result:
                result_list.append("False")
                print("Fail：開心捕魚自動下注失敗")
            else:
                try:
                    # 點擊老手模式
                    img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'JILI', game_list_dict["開心捕魚"][1])
                    img_path = img_coord.check_img()
                    click_result = img_coord.click_coordinate(img_path=img_path, total_time=20)
                    if not click_result:
                        print("無選擇模式，可無視Fail")

                except:
                    pass

                # 判斷是否進入下注區
                img_coord = img_recognition.CoordLocator(self.driver, 'Fish', 'JILI', game_list_dict["開心捕魚"][2])
                img_path = img_coord.check_img()
                click_result = img_coord.get_coordinate(target=img_path, canny=False)
                if not click_result:
                    result_list.append("False")
                else:
                    # 下注(畫面正中間)
                    ac.reset_actions()
                    ac.pause(1).move_by_offset(window_size['width'] / 2, window_size['height'] / 2)
                    ac.click_and_hold().pause(1).release().perform()

                    time.sleep(1)
                    print("***自動下注成功***")

            self.get_img("開心捕魚")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail:「開心捕魚」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class FishJILIAutoReport:
    """ JILI捕魚機自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 切換分頁-官網
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[0])

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', 'JILI')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'JILI')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-後台
        self.driver.switch_to.window(handles[1])

        # 取得後台報表資料
        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        manager_report, page_data, page_check_result = get_manage_report.search_report('3', 'JILI', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'JILI')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-超帳
        self.driver.switch_to.window(handles[-1])

        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', 'JILI', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'JILI', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], game_list_dict, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], game_list_dict, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
