# 自動下注&報表測試

## 介紹

1. 這個存儲庫進行 電子/捕魚遊戲/棋牌/動競 自動下注測試，確保 電子/捕魚遊戲/棋牌/動競 下注正常運作。
2. 下注完成後會進行 報表 對帳測試。

## 功能

- 針對不同的 廠商 各進桌自動下注測試。

## 先決條件

- Python 3.9.13 版本。

## 執行

1. 安裝所需的依賴：
    ```bash
    pip3 install -r requirements.txt
    ```

2. 執行測試
   使用以下命令運行自動下注測試：
    ```bash
    python3 Slot/slot_main.py  # 執行電子遊戲自動下注
    
    python3 Fish/fish_main.py  # 執行捕魚遊戲自動下注
   
    python3 Cards/cards_main.py  # 執行棋牌遊戲自動下注
   
    python3 PetRace/petRace_main.py  # 執行動競遊戲自動下注
    ```
