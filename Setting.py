# _*_ coding: UTF-8 _*_
import argparse

# 參數
parser = argparse.ArgumentParser()
parser.add_argument('-test_env', default='uat')  # 測試環境，預設值為 uat
parser.add_argument('-test_game', default='rsg')  # 測試遊戲，預設值為 rsg
parser.add_argument('-rsg_type', default='hot')  # rsg 種類，預設值為 hot
args, _ = parser.parse_known_args()  # 接收 command line 參數，不在預設的為 _
test_env = args.test_env
test_game = args.test_game
rsg_type = args.rsg_type

# 判斷測試環境
if test_env == "prod":
    # prod測試資料
    test_data = {
        "member_url": "https://bbbs.bacc1688.com/",  # 客端網址
        "account": ["Daa000001", "aa8888"],  # 客端帳號
        "api": {
            "w1": "http://tool.h1-system.com/backend/api/W1ApiHealth/GetW1ApiHealthQA",
            "h1": "http://tool.h1-system.com/backend/api/SwitchGame/GetSwitchQA",
        }
    }
elif test_env == "uat":
    # uat測試資料
    test_data = {
        "super_url": "https://acxwsz.royal-test.com/gank/hindex.jsp",  # 超帳網址
        "agent_url": "https://act.royal-test.com/manage/hindex.jsp",  # 代理端網址
        "member_url": "https://que.royal-test.com/",  # 客端網址
        "account": ["Aautotest", "6666"],  # 客端帳號
        "agent_account": ["aut0000", "6666"],  # 代理端帳號
        "super_account": ["suproyal", "6666"],  # 超帳帳號
        "shareholder_account": "autotest",  # 大股東帳號
        "api": {
            "w1": "https://tool.royal-test.com/backend/api/W1ApiHealth/GetW1ApiHealthQA",
            "h1": "https://tool.royal-test.com/backend/api/SwitchGame/GetSwitchQA",
        }
    }
