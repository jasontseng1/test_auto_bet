# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Lib import img_recognition
from Lib import get_report

game_list_dict = {
    "十三水": ["WinningThirteen_entry.png", "WinningThirteen_bet.png", "MT_continue.png"],
    "二人梭哈": ["ShowHand_entry.png", "ShowHand_bet.png", "MT_continue.png"]
}


class CardsMTAutoBet:
    """ MT棋牌自動下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_bet(self):
        """ 自動下注 """

        result_list = []

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='棋牌']]"))).click()

        # 點擊MT
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='MT']"))).click()

        # 十三水
        try:
            print("測試項目：「十三水」自動下注")
            # 點擊開啟十三水
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='十三水 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            count = 0
            while count < 2:
                # 進入桌別
                img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["十三水"][0])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    count += 1
                    self.driver.refresh()
                    print("\n重新判斷\n")
                else:
                    break

            if count == 2:
                result_list.append("False")
                print("Fail：十三水自動下注失敗")
            else:
                # 點擊比牌
                img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["十三水"][1])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    result_list.append("False")

                else:
                    # 判斷當局是否結束
                    img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["十三水"][2])
                    img_path = img_coord.check_img()
                    click_result = img_coord.get_coordinate(target=img_path, canny=False)
                    if not click_result:
                        result_list.append("False")

                    else:
                        print("***自動下注成功***")

            self.get_img("十三水")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail: 「十三水」自動下注失敗")
            print(e)
            result_list.append("False")

        # 二人梭哈
        try:
            print("測試項目：「二人梭哈」自動下注")
            # 點擊開啟二人梭哈
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='二人梭哈 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換分頁
            handles = self.driver.window_handles
            self.driver.switch_to.window(handles[-1])

            count = 0
            while count < 2:
                # 進入桌別
                img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["二人梭哈"][0])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    count += 1
                    self.driver.refresh()
                    print("\n重新判斷\n")
                else:
                    break

            if count == 2:
                result_list.append("False")
                print("Fail：二人梭哈自動下注失敗")
            else:
                # 點擊棄牌
                img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["二人梭哈"][1])
                img_path = img_coord.check_img()
                click_result = img_coord.click_coordinate(img_path=img_path)
                if not click_result:
                    result_list.append("False")

                else:
                    # 判斷當局是否結束
                    img_coord = img_recognition.CoordLocator(self.driver, 'Cards', 'MT', game_list_dict["二人梭哈"][2])
                    img_path = img_coord.check_img()
                    click_result = img_coord.get_coordinate(target=img_path, canny=False)
                    if not click_result:
                        result_list.append("False")

                    else:
                        print("***自動下注成功***")

            self.get_img("二人梭哈")
            time.sleep(1)

            self.driver.close()
            self.driver.switch_to.window(handles[0])

        except Exception as e:
            self.get_img("Fail: 「二人梭哈」自動下注失敗")
            print(e)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class CardsMTAutoReport:
    """ MT棋牌自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('5', 'MT')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'MT')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        # 登入代理端
        get_manage_report.login()
        # 取得後台報表資料
        manager_report, page_data, page_check_result = get_manage_report.search_report('5', 'MT', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'MT')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 登入超帳
        get_manage_report.super_login()
        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('5', 'MT', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'MT', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 遊戲清單
        new_game_list = []
        for game in game_list_dict.keys():
            new_game_list.append(game)

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], new_game_list, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], new_game_list, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results