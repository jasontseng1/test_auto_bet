# _*_ coding: UTF-8 _*_
import cv2
import time
import base64
import numpy as np
from pathlib import Path
from selenium.webdriver import ActionChains


class CoordLocator:

    def __init__(self, driver, game_type, game_vendor, img_name):
        self.driver = driver
        self.game_type = game_type  # 遊戲種類
        self.game_vendor = game_vendor  # 廠商
        self.img_name = img_name  # 圖片名稱

    def check_img(self):
        """
        確認是否有圖片

        Returns:
            (str|bool): 判斷成功回傳路徑，失敗則是 False
        """

        try:
            _dir = Path(__file__).parent.parent / f'{self.game_type}/{self.game_vendor}/images'
            target = str(_dir / f'{self.img_name}')

            # 判斷圖片是否存在
            if not Path(target).exists():
                print(f"Fail: 找無 {self.img_name} 此圖片")
                print(f"路徑: {target}")
                return False

            return target
        except:
            print("Fail: 判斷圖片路徑失敗")
            return False

    def get_coordinate(self, target, canny, re_load=False, total_time=60):
        """
        依據圖片取得座標
        參數:
            target (str): 圖片路徑
            canny (bool): 針對辨識圖片時是否多進行邊緣檢測
            re_load (bool): 是否進行重整來多判斷一次，預設為 False
            total_time (int): 辨識圖片的總時間，預設為 20

        Returns:
            (tuple|bool): 判斷成功回傳 x, y 座標 (1621.5 849.5)，失敗則是 False
        """

        try:
            threshold = 0.6
            try_sleep = 0
            re_try = 0

            # imread 有帶0 為灰階
            template = cv2.imread(target, 0)
            width, height = template.shape[::-1]

            while try_sleep < total_time:
                # 擷取當下畫面
                canvas_base64 = self.driver.get_screenshot_as_base64()
                canvas_png = base64.b64decode(canvas_base64)
                image = cv2.imdecode(np.frombuffer(canvas_png, np.uint8), 1)
                # 設定灰階
                img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

                if canny:
                    img_gray = cv2.GaussianBlur(img_gray, (5, 5), 0)
                    img_gray = cv2.Canny(img_gray, 50, 160)
                    template = cv2.GaussianBlur(template, (3, 3), 0)
                    template = cv2.Canny(template, 5, 160)

                res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
                _, max_v, _, max_location = cv2.minMaxLoc(res)

                if max_v < threshold:
                    time.sleep(5)
                    try_sleep += 5
                    if re_load:
                        if re_try == 0 and try_sleep == total_time:
                            re_try += 1
                            try_sleep = 0
                            self.driver.refresh()
                        elif re_try == 1 and try_sleep == total_time:
                            print(f"Fail: 判斷 {self.img_name} 圖片失敗")
                            return False
                    elif try_sleep == total_time:
                        print(f"Fail: 判斷 {self.img_name} 圖片失敗")
                        return False
                else:
                    x, y = max_location
                    return x + width / 2, y + height / 2
        except:
            print(f"Fail: {self.img_name} 判斷座標失敗")
            return False

    def click_coordinate(self, img_path, canny=False, right=False, re_load=False, total_time=60):
        """
        點擊座標
        參數:
            img_path (str): 圖片路徑
            canny (bool): 針對辨識圖片時是否多進行邊緣檢測，預設為 False
            right (bool): 是否使用右鍵點擊，預設為 False
            re_load (bool): 是否進行重整來多判斷一次，預設為 False
            total_time (int): 辨識圖片時的總時間，預設為 30

        Returns:
            (bool): 點擊座標失敗時回傳 False
        """

        try:
            coordinate = self.get_coordinate(img_path, canny, re_load, total_time)
            if coordinate:
                x, y = coordinate
                # 重製起始位置 0, 0
                ActionChains(self.driver).reset_actions()
                if right:
                    # 點擊右鍵
                    ActionChains(self.driver).pause(1).move_by_offset(x, y).context_click().perform()
                else:
                    # 點擊左鍵
                    ActionChains(self.driver).pause(1).move_by_offset(x, y).click().perform()

                ActionChains(self.driver).move_by_offset(-x, -y).perform()

                return True
            else:
                return False
        except:
            print(f"Fail: {self.img_name} 點擊座標失敗")
            return False
