# _*_ coding: UTF-8 _*_
import time
import decimal
import cv2
import pyotp
from pathlib import Path
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from Setting import *


def number_check(number):
    """
    確認數字
    參數:
        number (str|float): 數字

    Returns:
        (float): 判斷最後一位是否為 0，為 0 回傳當前數字，不為 0 捨棄最後一位
    """

    try:
        number = str(number)
        if '.' in number:
            last_number = number.split('.')[1]
            if len(last_number) >= 2:
                if last_number[-1] == '0':
                    return float(number)
                else:
                    return float(number[:-1])
            else:
                return float(number)
        else:
            return float(number)

    except Exception as e:
        print("Fail：確認數字失敗")
        print(e)
        return float(0)


def check_member_report_and_manager_report(member_report, manager_report, check_text='後台'):
    """
    確認官網報表資料與後台報表資料
    參數:
        member_report (list): 官網的報表注單資料
        manager_report (dict): 後台or超帳的報表注單資料
        check_text (str): 檢查項目文字，預設為後台

    Returns:
        (bool): 比對資料，成功為True，失敗or異常則是False
    """

    try:
        if not member_report or not manager_report:
            print("Fail：報表資料有誤")
            print(f"官網報表：{member_report}")
            print(f"{check_text}報表：{manager_report}\n")
            return False

        # init
        result_list = []

        print(f"==================== 官網報表與{check_text}報表 - 檢查開始 ====================\n")
        print(f"檢查項目：「官網報表」與「{check_text}報表」比對")

        for member_bet in member_report:
            for manager_bet in manager_report:
                if member_bet['編號'] == manager_bet['編號']:
                    member_key = list(member_bet['種類'].keys())[0]
                    manager_key = list(manager_bet['內容'].keys())[0]

                    # ======= 比對第一層 =======
                    # 比對下注金額
                    if member_bet['金額'] != manager_bet['下注金額']:
                        print(f"Fail：官網報表「下注金額」與{check_text}會員報表「下注金額」不同")
                        print(f"編號：{member_bet['編號']}")
                        print(f"官網報表「總下注金額」：{member_bet['金額']}")
                        print(f"{check_text}會員報表「總下注金額」：{manager_bet['下注金額']}\n")
                        result_list.append("False")

                    # 比對實投量
                    elif member_bet['實投量'] != manager_bet['實投量']:
                        print(f"Fail：官網報表「實投量」與{check_text}會員報表「實投量」不同")
                        print(f"編號：{member_bet['編號']}")
                        print(f"官網報表「實投量」：{member_bet['實投量']}")
                        print(f"{check_text}會員報表「實投量」：{manager_bet['實投量']}\n")
                        result_list.append("False")

                    # 比對輸贏金額
                    elif member_bet['輸贏'] != manager_bet['會員輸贏']:
                        print(f"Fail：官網報表「輸贏金額」與{check_text}會員報表「輸贏金額」不同")
                        print(f"編號：{member_bet['編號']}")
                        print(f"官網報表「輸贏金額」：{member_bet['輸贏']}")
                        print(f"{check_text}會員報表「輸贏金額」：{manager_bet['會員輸贏']}\n")
                        result_list.append("False")

                    # ======= 比對第二層 =======
                    for member_second_bet in member_bet['種類'][member_key]:
                        for manager_second_bet in manager_bet['內容'][manager_key]:
                            if member_second_bet['單號'] == manager_second_bet['交易單號']:
                                # 比對下注金額
                                if member_second_bet['金額'] != manager_second_bet['下注金額']:
                                    print(f"Fail：官網報表「下注金額」與{check_text}會員報表「下注金額」不同")
                                    print(f"編號：{member_bet['編號']}")
                                    print(f"單號：{member_second_bet['單號']}")
                                    print(f"官網報表「下注金額」：{member_second_bet['金額']}")
                                    print(f"{check_text}會員報表「下注金額」：{manager_second_bet['下注金額']}\n")
                                    result_list.append("False")

                                # 比對輸贏金額
                                elif member_second_bet['輸贏'] != manager_second_bet['輸贏金額']:
                                    print(f"Fail：官網報表「輸贏金額」與{check_text}會員報表「輸贏金額」不同")
                                    print(f"編號：{member_bet['編號']}")
                                    print(f"單號：{member_second_bet['單號']}")
                                    print(f"官網報表「輸贏金額」：{member_second_bet['輸贏']}")
                                    print(f"{check_text}會員報表「輸贏金額」：{manager_second_bet['輸贏金額']}\n")
                                    result_list.append("False")

        print("*** 檢查結束 ***\n")
        print(f"==================== 官網報表與{check_text}報表 - 檢查結束 ====================\n")

        results = False if "False" in str(result_list) else True
        return results

    except Exception as e:
        print(f"Fail：官網報表資料與{check_text}報表比對異常")
        print(e)
        return False


def check_game_list(game_data, default_data, check_text=''):
    """
    確認遊戲清單
    參數:
        game_data (list|dict): 遊戲名稱清單
        default_data (list|dict): 預期遊戲名稱清單
        check_text (str): 檢查項目文字，預設為空

    Returns:
        (bool): 比對遊戲清單正確為True，失敗or異常則是 False
    """

    try:
        print(f"檢查項目：「{check_text}遊戲清單」")

        # init
        game_list = game_data
        default_list = default_data

        # 判斷是否為 dict
        if isinstance(game_data, dict):
            game_list = list(game_data.keys())

        # 判斷是否為 dict
        if isinstance(default_data, dict):
            default_list = list(default_data.keys())

        # 判斷遊戲清單
        diff = list(set(default_list).difference(set(game_list)))
        if len(diff) > 0:
            print(f"Fail：{diff} 不在預期清單中")
            print(f"差異資料：{list(set(game_list).symmetric_difference(set(default_list)))}\n")
            result = False
        else:
            result = True

        print("*** 檢查結束 ***\n")

        return result

    except Exception as e:
        print(f"Fail：{check_text} 比對遊戲清單失敗")
        print(e)
        return False


class MemberReport:
    """ 官網報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def search_report(self, game_type='0', game_vendor='全部'):
        """
        搜尋報表
        參數:
            game_type (str): 遊戲種類，預設為 0
            game_vendor (str): 遊戲廠商，預設為 全部

        Returns:
            (list|bool): 查詢報表成功，回傳注單報表，失敗or無注單則是 False
        """

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//button[text()='會員中心']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='報表']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[text()='報表']")))
        time.sleep(1)

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[text()='種類']/../following-sibling::select")))
        # 種類 下拉選單
        game_type_select = Select(ele)

        # 下拉選單清單 (value:text):
        # 0:全部, 1:真人遊戲區, 3:電子遊戲區, 5:棋牌遊戲區, 4:體育遊戲區, 6:彩票遊戲區, 7:動物競技遊戲區, 8:電子競技遊戲
        # 取得下拉選單的 value 清單
        game_type_select_list = [i.get_attribute("value") for i in game_type_select.options]

        # 判斷是否有在清單內
        if game_type in game_type_select_list:
            # 選擇 value
            game_type_select.select_by_value(game_type)
            time.sleep(1)
        else:
            self.get_img(f"Fail：輸入的種類有誤 {game_type}")
            time.sleep(1)
            return False

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[text()='遊戲館別']/../following-sibling::select")))
        # 遊戲館別 下拉選單
        game_vendor_select = Select(ele)

        # 取得下拉選單的 text 清單
        game_vendor_select_list = [i.text for i in game_vendor_select.options]
        if len(game_vendor_select_list) == 1:
            time.sleep(2)
            game_vendor_select_list = [i.text for i in game_vendor_select.options]

        # 判斷是否有在清單內
        if game_vendor in game_vendor_select_list:
            # 選擇 text
            game_vendor_select.select_by_visible_text(game_vendor)
            time.sleep(1)
        else:
            self.get_img(f"Fail：遊戲館別 點擊 {game_vendor} 有誤")
            time.sleep(1)
            return False

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//button[text()='查詢']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//div[text()='時間']")))

        # 判斷是否有資料
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//td//span[@class='t-serial']")))
            time.sleep(1)
            report_data = self.format_report()
            return report_data

        except:
            try:
                ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//span[text()='總筆數']/following-sibling::span")))
                if int(ele.text) == 0:
                    self.get_img(f"Fail：種類-{game_type}，遊戲館別-{game_vendor}，帳號-{test_data['account'][0]} 查無資料")
                    time.sleep(1)
                    return False
            except Exception as e:
                self.get_img(f"Fail：種類-{game_type}，遊戲館別-{game_vendor}，帳號-{test_data['account'][0]} 查詢資料失敗")
                print(e)
                return False

    def format_report(self):
        """
        整理注單報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗or異常則是 False
        """

        try:
            # init
            bet_list = []
            field_dict = {}

            # 資料欄位
            table = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//table[@class='tb']")))
            th_list = table.find_elements(By.TAG_NAME, "th")
            for index, ele in enumerate(th_list):
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

            # 取得頁數資訊
            ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//span[@class='numberPage']")))
            now_page, total_page = ele.text.split('/')
            # 當前頁數
            now_page = int(now_page)
            # 總頁數
            total_page = int(total_page)

            for _ in range(total_page):
                _ele = self.driver.find_elements(By.XPATH, "//tbody/tr")

                for tr in _ele:
                    bet_data = {}
                    for i in field_dict:
                        # 時間要處裡換行
                        if i == '時間':
                            ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                            bet_data[i] = ele.text.replace('\n', ' ')
                        elif i == '種類':
                            ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                            bet_data[i] = {}
                            # 整理第二層報表
                            ele.click()
                            # wait loading
                            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
                            time.sleep(1)
                            bet_data[i][ele.text] = self.format_second_report()
                        else:
                            ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                            bet_data[i] = ele.text

                    bet_list.append(bet_data)

                if now_page != total_page:
                    count = 0
                    while count < 2:
                        # 換頁
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//a[@class='lnk_nextPage active']"))).click()
                        time.sleep(1)
                        _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, "//span[@class='numberPage']")))
                        # 新頁碼
                        new_page = int(_ele.text.split('/')[0])

                        if now_page == new_page:
                            count += 1
                            if count == 2:
                                self.get_img("Fail：注單換頁失敗")
                                return False
                        else:
                            now_page = new_page
                            count += 2

            self.get_img("官網報表")
            time.sleep(1)

            return bet_list

        except Exception as e:
            self.get_img("Fail：整理報表資料異常")
            print(e)
            return False

    def format_second_report(self):
        """
        整理注單第二層報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗則是 False
        """

        try:
            # init
            second_bet_list = []
            second_field_dict = {}

            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='reg_bet_content']//table[@class='tb']/tbody/tr")))

            # 資料欄位
            second_table = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='reg_bet_content']//table[@class='tb']")))
            second_th_list = second_table.find_elements(By.TAG_NAME, "th")
            for index, ele in enumerate(second_th_list):
                # +1 是給Xpath用
                second_field_dict[ele.text] = index + 1

            # 取得頁數資訊
            ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='reg_bet_content']//span[@class='numberPage']")))
            now_page, total_page = ele.text.split('/')
            # 當前頁數
            now_page = int(now_page)
            # 總頁數
            total_page = int(total_page)

            for _ in range(total_page):
                _ele = self.driver.find_elements(By.XPATH, "//div[@class='reg_bet_content']//tbody/tr")

                for tr in _ele:
                    second_bet_data = {}
                    for i in second_field_dict:
                        # 時間要處裡換行
                        if i == '下注時間' or i == '結算時間':
                            ele = tr.find_element(By.XPATH, f"./td[{second_field_dict[i]}]")
                            second_bet_data[i] = ele.text.replace('\n', ' ')
                        else:
                            ele = tr.find_element(By.XPATH, f"./td[{second_field_dict[i]}]")
                            second_bet_data[i] = ele.text

                    second_bet_list.append(second_bet_data)

                if now_page != total_page:
                    count = 0
                    while count < 2:
                        # 換頁
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@class='reg_bet_content']//a[@class='lnk_nextPage']"))).click()
                        time.sleep(1)
                        _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, "//div[@class='reg_bet_content']//span[@class='numberPage']")))
                        # 新頁碼
                        new_page = int(_ele.text.split('/')[0])

                        if now_page == new_page:
                            count += 1
                            if count == 2:
                                self.get_img("Fail：開牌紀錄換頁失敗")
                                return False
                        else:
                            now_page = new_page
                            count += 2

            # 關閉第二層報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='box_wrapper']//a[@class='box_close']"))).click()

            return second_bet_list

        except Exception as e:
            self.get_img("Fail：整理第二層報表異常")
            print(e)
            return False

    def check_web_and_report_data(self, bet_list, report_name=''):
        """
        確認官網統計資料與報表資料
        參數:
            bet_list (list): 報表注單資料
            report_name (str): 報表顯示名稱，預設為空

        Returns:
            (dict|bool): 回傳確認資料，異常則是False
        """

        try:
            if not bet_list:
                return False

            # init
            result_list = []
            check_data = {}

            # 統計比對數字
            # 報表總筆數
            report_total_number = len(bet_list)

            # 報表總下注金額
            report_total_bet_amount = 0
            for i in bet_list:
                report_total_bet_amount += decimal.Decimal(i['金額'])

            # 計算後在轉float避免顯示有誤
            report_total_bet_amount = float(report_total_bet_amount)

            # 報表總下注結果
            report_bet_win_lost = 0
            for i in bet_list:
                report_bet_win_lost += decimal.Decimal(i['輸贏'])

            # 計算後在轉float避免顯示有誤
            report_bet_win_lost = float(report_bet_win_lost)

            # 報表總實投量
            report_total_actual_bet_amount = 0
            for i in bet_list:
                report_total_actual_bet_amount += decimal.Decimal(i['實投量'])

            # 計算後在轉float避免顯示有誤
            report_total_actual_bet_amount = float(report_total_actual_bet_amount)

            print("==================== 官網報表 - 檢查開始 ====================\n")
            print("檢查項目：官網「總筆數」與報表「總筆數」比對")

            # 官網總筆數
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[text()='總筆數']/following-sibling::span")))
            total_number = int(ele.text)

            # 比對總筆數
            if total_number != report_total_number:
                print("Fail：官網「總筆數」與報表「總筆數」不同")
                print(f"官網「總筆數」：{total_number}")
                print(f"報表「總筆數」：{report_total_number}\n")
                result_list.append("False")
            else:
                check_data['總筆數'] = report_total_number

            print("*** 檢查結束 ***\n")

            print("檢查項目：官網「總下注金額」與報表「總下注金額」比對")

            # 總下注金額
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[text()='總下注金額']/following-sibling::span")))
            total_bet_amount = float(decimal.Decimal(ele.text))

            # 比對總下注金額
            if total_bet_amount != report_total_bet_amount:
                print("Fail：官網「總下注金額」與報表「總下注金額」不同")
                print(f"官網「總下注金額」：{total_bet_amount}")
                print(f"報表「總下注金額」：{report_total_bet_amount}\n")
                result_list.append("False")
            else:
                check_data['總下注金額'] = report_total_bet_amount

            print("*** 檢查結束 ***\n")

            print("檢查項目：官網「總下注結果」與報表「總下注結果」比對")

            # 總下注結果
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[text()='總下注結果']/following-sibling::span")))
            total_bet_win_lost = float(decimal.Decimal(ele.text))

            # 比對總下注結果
            if total_bet_win_lost != report_bet_win_lost:
                print("Fail：官網「總下注結果」與報表「總下注結果」不同")
                print(f"官網「總下注結果」：{total_bet_win_lost}")
                print(f"報表「總下注結果」：{report_bet_win_lost}\n")
                result_list.append("False")
            else:
                check_data['總下注結果'] = report_bet_win_lost

            print("*** 檢查結束 ***\n")

            print("檢查項目：官網「總實投量」與報表「總實投量」比對")

            # 總實投量
            ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[text()='總實投量']/following-sibling::span")))
            total_actual_bet_amount = float(decimal.Decimal(ele.text))

            # 比對總實投量
            if total_actual_bet_amount != report_total_actual_bet_amount:
                print("Fail：官網「總實投量」與報表「總實投量」不同")
                print(f"官網「總實投量」：{total_actual_bet_amount}")
                print(f"報表「總實投量」：{report_total_actual_bet_amount}\n")
                result_list.append("False")
            else:
                check_data['總實投量'] = report_total_actual_bet_amount

            print("*** 檢查結束 ***\n")

            print("檢查項目：「報表種類名稱」")

            if report_name:
                # 取第一筆來比對
                _name = list(bet_list[0]['種類'].keys())[0]
                # 判斷顯示的名稱
                if _name != report_name:
                    print("Fail：報表名稱與預期名稱不同")
                    print(f"報表名稱：{_name}")
                    print(f"預期名稱：{report_name}")
                    result_list.append("False")
            else:
                _name_list = []
                for i in bet_list:
                    _name = list(i['種類'].keys())[0]
                    if _name not in _name_list:
                        _name_list.append(_name)

                print("無帶入預期名稱")
                print(f"報表名稱：{_name_list}\n")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「會員報表第一層」與「會員報表第二層」比對")

            for bet in bet_list:
                _name = list(bet['種類'].keys())[0]
                # 確認第一層與第二層筆數是否相同
                if int(bet['內容'].split('總筆數:')[1]) != len(bet['種類'][_name]):
                    print("Fail：第一層「筆數」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「筆數」：{bet['內容'].split('總筆數:')[1]}")
                    print(f"第二層「筆數」：{len(bet['種類'][_name])}\n")
                    result_list.append("False")

                # 確認第一層與第二層金額、輸贏是否相同
                first_bet = decimal.Decimal(bet['金額'])
                first_win_lost = decimal.Decimal(bet['輸贏'])
                second_bet = 0
                second_win_lost = 0

                # 計算第二層下注金額、輸贏金額
                for bet_second in bet['種類'][_name]:
                    second_bet += decimal.Decimal(bet_second['金額'])
                    second_win_lost += decimal.Decimal(bet_second['輸贏'])

                # 計算後在轉float避免顯示有誤
                first_bet = float(first_bet)
                first_win_lost = float(first_win_lost)
                second_bet = float(second_bet)
                second_win_lost = float(second_win_lost)

                # 比對下注金額
                if first_bet != second_bet:
                    print("Fail：第一層「下注金額」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「下注金額」：{first_bet}")
                    print(f"第二層「下注金額」：{second_bet}\n")
                    result_list.append("False")

                # 比對輸贏金額
                elif first_win_lost != second_win_lost:
                    print("Fail：第一層「輸贏金額」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「輸贏金額」：{first_win_lost}")
                    print(f"第二層「輸贏金額」：{second_win_lost}\n")
                    result_list.append("False")

            print("*** 檢查結束 ***\n")
            print("==================== 官網報表 - 檢查結束 ====================\n")

            check_data['確認結果'] = False if "False" in str(result_list) else True
            return check_data

        except Exception as e:
            self.get_img("Fail：確認官網統計資料與報表資料異常")
            print(e)
            return False


class ManageReport:
    """ 後端報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def login(self):
        """ 登入 """

        try:
            # 開新網址
            new_window = f"window.open('{test_data['agent_url']}');"

            self.driver.execute_script(new_window)
            time.sleep(2)
            handles = self.driver.window_handles
            time.sleep(1)
            self.driver.switch_to.window(handles[-1])

            input_acc = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//span[contains(text(), '管理帳號:')]/following-sibling::input")))
            input_acc.send_keys(test_data['agent_account'][0])

            input_pwd = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@type='password']")))
            input_pwd.send_keys(test_data['agent_account'][1])

            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//a[./img[@src='image/btn3_Big5.jpg']]"))).click()
            time.sleep(1)

            # 會跳兩次彈窗
            self.driver.switch_to.alert.accept()
            self.driver.switch_to.alert.accept()

            self.get_img("後台登入成功")
            return True
        except:
            self.get_img("後台登入失敗")
            return False

    def super_login(self):
        """ 超帳登入 """

        try:
            # 開新網址
            new_window = f"window.open('{test_data['super_url']}');"
            self.driver.execute_script(new_window)
            time.sleep(2)
            handles = self.driver.window_handles
            time.sleep(1)
            self.driver.switch_to.window(handles[-1])

            img_name = 'super_qrcode.png'
            _dir = Path(__file__).parent / 'qrcode_images'
            target = str(_dir / img_name)

            # 判斷圖片是否存在
            if not Path(target).exists():
                print(f"Fail: 找無 {img_name} 此圖片")
                print(f"路徑: {target}")
                return False

            img = cv2.imread(target)
            detect = cv2.QRCodeDetector()
            value, _, _ = detect.detectAndDecode(img)
            search_text = 'secret='

            if search_text in value:
                text_start = value.find(search_text)
                text_end = text_start + value[text_start:].find('&')
                _, key = value[text_start:text_end].split('=')
                totp = pyotp.TOTP(key)
            else:
                print(f"Fail：圖片有誤 {target}")
                return False

            count = 0
            while count < 2:
                input_acc = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//input[@name='user']")))
                input_acc.send_keys(test_data['super_account'][0])

                input_pwd = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//input[@name='pass']")))
                input_pwd.send_keys(test_data['super_account'][1])

                input_auth = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//input[@name='auth']")))
                input_auth.send_keys(totp.now())

                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//input[@name='submit']"))).click()
                time.sleep(1)

                try:
                    alert = self.driver.switch_to.alert
                    if '驗證碼錯誤' in alert.text:
                        count += 1
                        if count == 2:
                            print("Fail：超帳登入失敗")
                            print(f"失敗訊息：{alert.text}")
                            return False

                        alert.accept()
                        self.driver.refresh()
                except:
                    count += 2
                    pass

                self.get_img("超帳登入成功")
                return True
        except:
            self.get_img("後台登入失敗")
            return False

    def search_report(self, game_type='0', game_vendor='全部', check_data=''):
        """
        搜尋報表
        參數:
            game_type (str): 遊戲種類，預設為 0
            game_vendor (str): 遊戲廠商，預設為 全部
            check_data (dict): 官網的確認資料，預設為空

        Returns:
            Tuple[list, dict, bool|bool, bool, bool]: 查詢報表成功，回傳注單報表, 頁面統計資料, 頁面檢查成功，失敗or無注單則是 False
        """

        # init
        field_dict = {}
        result = True

        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
            (By.XPATH, "//frame[@name='topFrame']")))

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//a[text()='報表']"))).click()
        time.sleep(1)

        self.driver.switch_to.default_content()

        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
            (By.XPATH, "//frame[@name='mainFrame']")))

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//select[@id='Game_Class']")))
        # 種類 下拉選單
        game_type_select = Select(ele)

        # 下拉選單清單 (value:text):
        # 0:全部, 1:真人遊戲區, 3:電子遊戲區, 4:體育遊戲區, 5:棋牌遊戲區, 6:彩票遊戲區, 7:動物競技遊戲區, 8:電子競技遊戲
        # 取得下拉選單的 value 清單
        game_type_select_list = [i.get_attribute("value") for i in game_type_select.options]

        # 判斷是否有在清單內
        if game_type in game_type_select_list:
            # 選擇 value
            game_type_select.select_by_value(game_type)
            time.sleep(1)
        else:
            self.get_img(f"Fail：輸入的種類有誤 {game_type}")
            time.sleep(1)
            return False, False, False

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//select[@id='ThirdParty_Id']")))
        # 遊戲館別 下拉選單
        game_vendor_select = Select(ele)

        # 取得下拉選單的 text 清單
        game_vendor_select_list = [i.text for i in game_vendor_select.options]
        if len(game_vendor_select_list) == 1:
            time.sleep(2)
            game_vendor_select_list = [i.text for i in game_vendor_select.options]

        # 判斷是否有在清單內
        if game_vendor in game_vendor_select_list:
            # 選擇 text
            game_vendor_select.select_by_visible_text(game_vendor)
            time.sleep(1)
        else:
            self.get_img(f"Fail：遊戲館別 點擊 {game_vendor} 有誤")
            time.sleep(1)
            return False, False, False

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//input[@name='SUBMIT']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//font[text()='種類:']")))
        time.sleep(1)

        # 判斷是否有資料
        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, f"//td[text()='{test_data['account'][0]}']")))
            time.sleep(1)

            # 資料欄位
            tr_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//td[./font[text()='會員帳號']]/..")))
            td_list = tr_list.find_elements(By.TAG_NAME, "td")
            for index, ele in enumerate(td_list):
                # 取特定欄位
                if ele.text in ['筆數', '下注金額', '實投量', '會員輸贏']:
                    # +1 是給Xpath用
                    field_dict[ele.text] = index + 1

            # 檢查搜尋結果頁面
            page_data, page_check_result = self.check_search_page(field_dict, check_data)
            if not page_check_result:
                result = False

            # 點擊下注金額來進入報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, f"//td[text()='{test_data['account'][0]}']/../td[{field_dict['下注金額']}]/a"))).click()
            time.sleep(1)
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//font[text()='代理商結果']")))

            try:
                # 設定顯示全部
                ele = WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable(
                    (By.XPATH, "//select[@name='Row_Set']")))
                # 筆數 下拉選單
                row_select = Select(ele)
                time.sleep(1)
                row_select.select_by_visible_text('全部')
                time.sleep(1)
            except:
                pass

            report_data = self.format_report()

            return report_data, page_data, result

        except Exception as e:
            self.get_img(f"Fail：種類-{game_type}，遊戲館別-{game_vendor}，帳號-{test_data['account'][0]} 查無資料")
            print(e)
            return False, False, False

    def super_search_report(self, game_type='0', game_vendor='全部', check_data=''):
        """
        搜尋報表
        參數:
            game_type (str): 遊戲種類，預設為 0
            game_vendor (str): 遊戲廠商，預設為 全部
            check_data (dict): 官網的確認資料，預設為空

        Returns:
            Tuple[list, dict, bool|bool, bool, bool]: 查詢報表成功，回傳注單報表, 頁面統計資料, 頁面檢查成功，失敗or無注單則是 False
        """

        # init
        field_dict = {}
        result = True

        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
            (By.XPATH, "//frame[@name='topFrame']")))

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//a[text()='報表']"))).click()
        time.sleep(1)

        self.driver.switch_to.default_content()

        WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it(
            (By.XPATH, "//frame[@name='mainFrame']")))

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//select[@id='Game_Class']")))
        # 種類 下拉選單
        game_type_select = Select(ele)

        # 下拉選單清單 (value:text):
        # 0:全部, 1:真人遊戲區, 3:電子遊戲區, 4:體育遊戲區, 5:棋牌遊戲區, 6:彩票遊戲區, 7:動物競技遊戲區, 8:電子競技遊戲
        # 取得下拉選單的 value 清單
        game_type_select_list = [i.get_attribute("value") for i in game_type_select.options]

        # 判斷是否有在清單內
        if game_type in game_type_select_list:
            # 選擇 value
            game_type_select.select_by_value(game_type)
            time.sleep(1)
        else:
            self.get_img(f"Fail：輸入的種類有誤 {game_type}")
            time.sleep(1)
            return False, False, False

        ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//select[@id='ThirdParty_Id']")))
        # 遊戲館別 下拉選單
        game_vendor_select = Select(ele)

        # 取得下拉選單的 text 清單
        game_vendor_select_list = [i.text for i in game_vendor_select.options]
        if len(game_vendor_select_list) == 1:
            time.sleep(2)
            game_vendor_select_list = [i.text for i in game_vendor_select.options]

        # 判斷是否有在清單內
        if game_vendor in game_vendor_select_list:
            # 選擇 text
            game_vendor_select.select_by_visible_text(game_vendor)
            time.sleep(1)
        else:
            self.get_img(f"Fail：遊戲館別 點擊 {game_vendor} 有誤")
            time.sleep(1)
            return False, False, False

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//input[@name='SUBMIT']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//font[text()='種類:']")))
        time.sleep(1)

        # 一層一層進入
        for i in ['總站名稱', '組別名稱', '股東名稱', '總代理名稱', '代理商結果', '下注金額結果']:
            # 判斷是否有資料
            try:
                if i == '總站名稱':
                    WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                        (By.XPATH, f"//td[text()='{test_data['super_account'][0]}']")))

                # 大股東層判斷有無顯示
                elif i == '組別名稱':
                    WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                        (By.XPATH, f"//td[text()='{test_data['shareholder_account']}']")))

                # 會員判斷有無顯示
                elif i == '下注金額結果':
                    WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                        (By.XPATH, f"//td[text()='{test_data['account'][0]}']")))

                time.sleep(1)

                # 資料欄位
                tr_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                    (By.XPATH, f"//td[./font[text()='{i}']]/..")))
                td_list = tr_list.find_elements(By.TAG_NAME, "td")
                _index = 0
                for index, ele in enumerate(td_list):
                    # 取特定欄位
                    if ele.text == '下注金額':
                        # +1 是給Xpath用
                        _index = index + 1

                if i == '總站名稱':
                    # 點擊下注金額來進入報表
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//td[text()='{test_data['super_account'][0]}']/../td[{_index}]/a"))).click()

                elif i == '組別名稱':
                    # 點擊下注金額來進入報表
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//td[text()='{test_data['shareholder_account']}']/../td[{_index}]/a"))).click()

                elif i == '下注金額結果':
                    # 資料欄位
                    tr_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                        (By.XPATH, "//td[./font[text()='會員帳號']]/..")))
                    td_list = tr_list.find_elements(By.TAG_NAME, "td")
                    for index, ele in enumerate(td_list):
                        # 取特定欄位
                        if ele.text in ['筆數', '下注金額', '實投量', '會員']:
                            # +1 是給Xpath用
                            field_dict[ele.text] = index + 1

                    # 檢查搜尋結果頁面
                    page_data, page_check_result = self.check_search_page(field_dict, check_data, check_text='超帳')
                    if not page_check_result:
                        result = False

                    # 點擊下注金額來進入報表
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//td[text()='{test_data['account'][0]}']/../td[{field_dict['下注金額']}]/a"))).click()
                    time.sleep(1)
                    WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                        (By.XPATH, f"//font[text()='{i}']")))

                    try:
                        # 設定顯示全部
                        ele = WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable(
                            (By.XPATH, "//select[@name='Row_Set']")))
                        # 筆數 下拉選單
                        row_select = Select(ele)
                        time.sleep(1)
                        row_select.select_by_visible_text('全部')
                        time.sleep(1)
                    except:
                        pass

                    report_data = self.format_report(check_text='超帳')

                    return report_data, page_data, result

                else:
                    # 點擊下注金額來進入報表
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//td[./font[text()='{i}']]/../following-sibling::tr[1]/td[{_index}]/a"))).click()

                time.sleep(1)
                WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                    (By.XPATH, f"//font[text()='{i}']")))

            except Exception as e:
                if i == '總站名稱':
                    self.get_img(f"Fail：種類-{game_type}，遊戲館別-{game_vendor}，帳號-{test_data['super_account'][0]} 查無資料")
                elif i == '組別名稱':
                    self.get_img(f"Fail：大股東帳號-{test_data['shareholder_account']} 查無資料")
                elif i == '下注金額結果':
                    self.get_img(f"Fail：會員帳號-{test_data['account'][0]} 查無資料")
                else:
                    self.get_img(f"Fail：{i} 查無資料")
                print(e)
                return False, False, False

    def check_search_page(self, field_dict, check_data, check_text='後台'):
        """
        確認搜尋結果頁面統計資料
        參數:
            field_dict (dict): 欄位資料
            check_data (dict): 官網的確認資料
            check_text (str): 檢查項目文字，預設為後台

        Returns:
             Tuple[dict|dict, bool|bool, bool]:  確認搜尋結果頁面統計資料，比對成功回傳頁面統計資料，失敗or異常則是 False
        """

        try:
            # init
            page_data = {}
            result_list = []

            print(f"==================== {check_text}報表 - 檢查開始 ====================\n")

            # 取得搜尋頁面資料
            for i in field_dict:
                _ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                    (By.XPATH, f"//tr[./td[text()='{test_data['account'][0]}']]/td[{field_dict[i]}]")))
                if i == '筆數':
                    page_data[i] = int(_ele.text)
                else:
                    page_data[i] = float(_ele.text)

            # 檢查check_data
            if check_data:
                check_result = all(key in check_data for key in ['總筆數', '總下注金額', '總下注結果', '總實投量'])
            else:
                check_result = False

            # 有資料才進行比對
            if check_result:
                print(f"檢查項目：「官網總計」與「{check_text}總計」比對")

                # 資料處裡
                total_bet = number_check(check_data['總下注金額'])
                total_win_lose = number_check(check_data['總下注結果'])
                total_actual_bet = number_check(check_data['總實投量'])

                # 比對官網資料
                if page_data['筆數'] != check_data['總筆數']:
                    print(f"Fail：官網「總筆數」與{check_text}不同")
                    print(f"官網「總筆數」：{check_data['總筆數']}")
                    print(f"{check_text}「總筆數」：{page_data['筆數']}\n")
                    result_list.append("False")

                elif page_data['下注金額'] != total_bet:
                    print(f"Fail：官網「總下注金額」與{check_text}不同")
                    print(f"官網「總下注金額」：{total_bet}")
                    print(f"{check_text}「總下注金額」：{page_data['下注金額']}\n")
                    result_list.append("False")

                elif page_data['實投量'] != total_actual_bet:
                    print(f"Fail：官網「總實投量」與{check_text}不同")
                    print(f"官網「總實投量」：{total_actual_bet}")
                    print(f"{check_text}「總實投量」：{page_data['實投量']}\n")
                    result_list.append("False")

                # 超帳輸贏欄位名稱不同
                if check_text == '超帳':
                    if page_data['會員'] != total_win_lose:
                        print(f"Fail：官網「會員輸贏」與{check_text}不同")
                        print(f"官網「會員輸贏」：{total_win_lose}")
                        print(f"{check_text}「會員輸贏」：{page_data['會員']}\n")
                        result_list.append("False")

                else:
                    if page_data['會員輸贏'] != total_win_lose:
                        print(f"Fail：官網「會員輸贏」與{check_text}不同")
                        print(f"官網「會員輸贏」：{total_win_lose}")
                        print(f"{check_text}「會員輸贏」：{page_data['會員輸贏']}\n")
                        result_list.append("False")

                print("*** 檢查結束 ***\n")

                self.get_img(f"{check_text}搜尋結果頁面")
                time.sleep(1)

                results = False if "False" in str(result_list) else True
                return page_data, results

            else:
                return page_data, True

        except Exception as e:
            self.get_img("Fail：確認搜尋結果頁面資料異常")
            print(e)
            return False, False

    def format_report(self, check_text='後台'):
        """
        整理注單報表
        參數:
            check_text (str): 檢查項目文字，預設為後台

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗or異常則是 False
        """

        try:
            # init
            bet_list = []
            field_dict = {}

            # 資料欄位
            tr = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//font[text()='會員帳號']/../..")))
            td_list = tr.find_elements(By.TAG_NAME, "td")
            for index, ele in enumerate(td_list):
                # +1 是給Xpath用
                field_dict[ele.text] = index + 1

            _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located(
                (By.XPATH, f"//td[text()='{test_data['account'][0]}']/..")))

            for tr in _ele:
                bet_data = {}
                for i in field_dict:
                    # 時間要處裡換行
                    if i == '時間':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        _number, _time = ele.text.split('\n')
                        bet_data['編號'] = _number
                        bet_data[i] = _time
                    elif i == '內容':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]/a")
                        bet_data[i] = {}
                        # 整理第二層報表
                        ele.click()
                        time.sleep(1)
                        bet_data[i][ele.text] = self.format_second_report()
                    elif i == '會員輸贏':
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = format(float(ele.text), '.2f')
                    else:
                        ele = tr.find_element(By.XPATH, f"./td[{field_dict[i]}]")
                        bet_data[i] = ele.text

                bet_list.append(bet_data)

            self.get_img(f"{check_text}報表")
            time.sleep(1)

            return bet_list

        except Exception as e:
            print(e)
            self.get_img("Fail：整理報表資料異常")
            return False

    def format_second_report(self):
        """
        整理注單第二層報表

        Returns:
            (list|bool): 回傳整理後的注單報表，失敗則是 False
        """

        try:
            # init
            second_bet_list = []
            second_field_dict = {}

            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//font[@id='xx1']")))

            # 資料欄位
            second_table = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//font[@id='xx1']//td[text()='交易單號']/..")))
            second_td_list = second_table.find_elements(By.TAG_NAME, "td")
            for index, ele in enumerate(second_td_list):
                # +1 是給Xpath用
                second_field_dict[ele.text] = index + 1

            _ele = self.driver.find_elements(By.XPATH, "//font[@id='xx1']//tr[@align='right']")

            for tr in _ele:
                second_bet_data = {}
                for i in second_field_dict:
                    ele = tr.find_element(By.XPATH, f"./td[{second_field_dict[i]}]")
                    second_bet_data[i] = ele.text

                second_bet_list.append(second_bet_data)

            # 關閉第二層報表
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@id='divDetail1']//a[./img[@src='edit_dot.gif']]"))).click()

            return second_bet_list

        except:
            self.get_img("Fail：整理第二層報表異常")
            return False

    def check_web_and_report_data(self, bet_list, page_data, report_name='', check_super=False):
        """
        確認後台統計資料與報表資料
        參數:
            bet_list (list): 報表注單資料
            page_data (dict): 頁面統計資料
            report_name (str): 報表顯示名稱，預設為空
            check_super (bool): 是否為確認超帳，預設為False

        Returns:
            (dict|bool): 回傳確認資料，異常則是False
        """

        try:
            if not bet_list:
                return False

            # init
            result_list = []
            manage_check_data = {}

            # 統計比對數字
            # 報表總筆數
            report_total_number = len(bet_list)

            # 報表總下注金額
            report_total_bet_amount = 0
            for i in bet_list:
                report_total_bet_amount += decimal.Decimal(i['下注金額'])

            # 計算後在轉float避免顯示有誤
            report_total_bet_amount = float(report_total_bet_amount)

            # 報表總下注結果
            report_bet_win_lost = 0
            for i in bet_list:
                report_bet_win_lost += decimal.Decimal(i['會員輸贏'])

            # 計算後在轉float避免顯示有誤
            report_bet_win_lost = float(report_bet_win_lost)

            # 報表總實投量
            report_total_actual_bet_amount = 0
            for i in bet_list:
                report_total_actual_bet_amount += decimal.Decimal(i['實投量'])

            # 計算後在轉float避免顯示有誤
            report_total_actual_bet_amount = number_check(float(report_total_actual_bet_amount))

            if page_data:
                print("檢查項目：「搜尋結果頁面」與「會員報表頁面」比對")

                # 會員報表頁面總筆數
                ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//span[@id='bishuJ']")))
                total_number = int(ele.text)

                # 比對總筆數
                if total_number != page_data['筆數']:
                    print("Fail：會員報表「總筆數」與搜尋結果「總筆數」不同")
                    print(f"會員報表「總筆數」：{total_number}")
                    print(f"搜尋結果「總筆數」：{page_data['筆數']}\n")
                    result_list.append("False")

                # 總下注金額
                ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//span[@id='jinerJ']")))
                total_bet_amount = float(decimal.Decimal(ele.text))

                # 比對總下注金額
                if total_bet_amount != page_data['下注金額']:
                    print("Fail：會員報表「總下注金額」與搜尋結果「總下注金額」不同")
                    print(f"會員報表「總下注金額」：{total_bet_amount}")
                    print(f"搜尋結果「總下注金額」：{page_data['下注金額']}\n")
                    result_list.append("False")

                # 總下注結果
                ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//td[@class='m_tline']//font[contains(text(), '筆數:')]")))
                total_bet_win_lost = float(decimal.Decimal(ele.text.split('會員結果:')[1]))

                # 是否為超帳
                if check_super:
                    # 比對總下注結果
                    if total_bet_win_lost != page_data['會員']:
                        print("Fail：會員報表「總下注結果」與搜尋結果「總下注結果」不同")
                        print(f"會員報表「總下注結果」：{total_bet_win_lost}")
                        print(f"搜尋結果「總下注結果」：{page_data['會員']}\n")
                        result_list.append("False")
                else:
                    # 比對總下注結果
                    if total_bet_win_lost != page_data['會員輸贏']:
                        print("Fail：會員報表「總下注結果」與搜尋結果「總下注結果」不同")
                        print(f"會員報表「總下注結果」：{total_bet_win_lost}")
                        print(f"搜尋結果「總下注結果」：{page_data['會員輸贏']}\n")
                        result_list.append("False")

                print("*** 檢查結束 ***\n")

                print("檢查項目：「會員報表上方總計」與「會員報表下方總計」比對")

                # 比對會員報表頁面上方總計與下方總計
                ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//tr[@class='rp_top2']")))
                data_list = ele.text.split(' ')[:3]
                # 資料處裡
                under_total_number = int(data_list[0])
                under_total_bet = data_list[1]
                under_total_win_lose = data_list[2]

                # 比對官網資料
                if total_number != under_total_number:
                    print("Fail：會員報表上方「總筆數」與會員報表下方不同")
                    print(f"會員報表上方「總筆數」：{total_number}")
                    print(f"會員報表下方「總筆數」：{under_total_number}\n")
                    result_list.append("False")

                # 報表上方只顯示小數點第一位，故需處理比對數字
                elif total_bet_amount != float(under_total_bet[:-1]):
                    print("Fail：會員報表上方「總下注金額」與會員報表下方不同")
                    print(f"會員報表上方「總下注金額」：{total_bet_amount}")
                    print(f"會員報表下方「總下注金額」：{float(under_total_bet[:-1])}\n")
                    result_list.append("False")

                # 報表上方只顯示小數點第一位，故需處理比對數字
                elif total_bet_win_lost != float(under_total_win_lose[:-1]):
                    print("Fail：會員報表上方「會員輸贏」與會員報表下方不同")
                    print(f"會員報表上方「會員輸贏」：{total_bet_win_lost}")
                    print(f"會員報表下方「會員輸贏」：{float(under_total_win_lose[:-1])}\n")
                    result_list.append("False")

                print("*** 檢查結束 ***\n")

                print("檢查項目：「會員報表總計」與「報表下方總計」比對")

                # 比對總筆數
                if under_total_number != report_total_number:
                    print("Fail：會員報表下方「總筆數」與報表「總筆數」不同")
                    print(f"會員報表下方「總筆數」：{under_total_number}")
                    print(f"報表「總筆數」：{report_total_number}\n")
                    result_list.append("False")
                else:
                    manage_check_data['總筆數'] = report_total_number

                # 比對總下注金額
                if float(under_total_bet) != report_total_bet_amount:
                    print("Fail：會員報表下方「總下注金額」與報表「總下注金額」不同")
                    print(f"會員報表下方「總下注金額」：{float(under_total_bet)}")
                    print(f"報表「總下注金額」：{report_total_bet_amount}\n")
                    result_list.append("False")
                else:
                    manage_check_data['總下注金額'] = report_total_bet_amount

                # 比對總下注結果
                if float(under_total_win_lose) != report_bet_win_lost:
                    print("Fail：會員報表下方「總下注結果」與報表「總下注結果」不同")
                    print(f"會員報表下方「總下注結果」：{float(under_total_win_lose)}")
                    print(f"報表「總下注結果」：{report_bet_win_lost}\n")
                    result_list.append("False")
                else:
                    manage_check_data['總下注結果'] = report_bet_win_lost

                print("*** 檢查結束 ***\n")

                print("檢查項目：「搜尋結果總實投量」與「會員報表總實投量」比對")

                # 比對總實投量
                if page_data['實投量'] != report_total_actual_bet_amount:
                    print("Fail：後台「總實投量」與報表「總實投量」不同")
                    print(f"後台「總實投量」：{page_data['實投量']}")
                    print(f"報表「總實投量」：{report_total_actual_bet_amount}\n")
                    result_list.append("False")
                else:
                    manage_check_data['總實投量'] = report_total_actual_bet_amount

                print("*** 檢查結束 ***\n")
            else:
                manage_check_data['總筆數'] = report_total_number
                manage_check_data['總下注金額'] = report_total_bet_amount
                manage_check_data['總下注結果'] = report_bet_win_lost
                manage_check_data['總實投量'] = report_total_actual_bet_amount

            print("檢查項目：「報表種類名稱」")

            if report_name:
                # 取第一筆來比對
                _name = bet_list[0]['種類']
                # 判斷顯示的名稱
                if _name != report_name:
                    print("Fail：報表名稱與預期名稱不同")
                    print(f"報表名稱：{_name}")
                    print(f"預期名稱：{report_name}")
                    result_list.append("False")
            else:
                _name_list = []
                for i in bet_list:
                    _name = i['種類']
                    if _name not in _name_list:
                        _name_list.append(_name)

                print("無帶入預期名稱")
                print(f"報表名稱：{_name_list}\n")

            print("*** 檢查結束 ***\n")

            print("檢查項目：「會員報表第一層」與「會員報表第二層」比對")

            for bet in bet_list:
                number_key = list(bet['內容'].keys())[0]
                first_number = int(number_key.split('總筆數:')[1])
                # 確認第一層與第二層筆數是否相同
                if first_number != len(bet['內容'][number_key]):
                    print("Fail：第一層「筆數」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「筆數」：{first_number}")
                    print(f"第二層「筆數」：{len(bet['內容'][number_key])}\n")
                    result_list.append("False")

                # 確認第一層與第二層金額、輸贏是否相同
                first_bet = decimal.Decimal(bet['下注金額'])
                first_win_lost = decimal.Decimal(bet['會員輸贏'])
                second_bet = 0
                second_win_lost = 0

                # 計算第二層下注金額、輸贏金額
                for bet_second in bet['內容'][number_key]:
                    second_bet += decimal.Decimal(bet_second['下注金額'])
                    second_win_lost += decimal.Decimal(bet_second['輸贏金額'])

                # 計算後在轉float避免顯示有誤
                first_bet = float(first_bet)
                first_win_lost = float(first_win_lost)
                second_bet = float(second_bet)
                second_win_lost = float(second_win_lost)

                # 比對下注金額
                if first_bet != second_bet:
                    print("Fail：第一層「下注金額」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「下注金額」：{first_bet}")
                    print(f"第二層「下注金額」：{second_bet}\n")
                    result_list.append("False")

                # 比對輸贏金額
                elif first_win_lost != second_win_lost:
                    print("Fail：第一層「輸贏金額」與第二層不同")
                    print(f"編號：{bet['編號']}")
                    print(f"第一層「輸贏金額」：{first_win_lost}")
                    print(f"第二層「輸贏金額」：{second_win_lost}\n")
                    result_list.append("False")

            print("*** 檢查結束 ***\n")
            print("==================== 後台報表 - 檢查結束 ====================\n")

            manage_check_data['確認結果'] = False if "False" in str(result_list) else True

            return manage_check_data

        except Exception as e:
            self.get_img("Fail：確認後台統計資料與報表資料異常")
            print(e)
            return False
