# _*_ coding: UTF-8 _*_
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from RSG import Slot_RSG
from JDB import Slot_JDB
from DS import Slot_DS
from GEMINI import Slot_GEMINI
from JOKER import Slot_JOKER
from Ameba import Slot_Ameba
from TP import Slot_TP
from GR import Slot_GR
from NS import Slot_NS
from MG import Slot_MG
from JILI import Slot_JILI
from PP import Slot_PP
from FC import Slot_FC


options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()

# 官網視窗id
member_window = driver.current_window_handle


class SlotBet(unittest.TestCase):
    """ 電子下注 """

    s_rsg_ab = Slot_RSG.SlotRSGAutoBet(driver, get_img, test_env, rsg_type, member_window)
    s_jdb_ab = Slot_JDB.SlotJDBAutoBet(driver, get_img, member_window)
    s_ds_ab = Slot_DS.SlotDSAutoBet(driver, get_img, member_window)
    s_gemini_ab = Slot_GEMINI.SlotGEMINIAutoBet(driver, get_img, member_window)
    s_joker_ab = Slot_JOKER.SlotJOKERAutoBet(driver, get_img, member_window)
    s_ambea_ab = Slot_Ameba.SlotAmebaAutoBet(driver, get_img, member_window)
    s_tp_ab = Slot_TP.SlotTPAutoBet(driver, get_img, member_window)
    s_gr_ab = Slot_GR.SlotGRAutoBet(driver, get_img, member_window)
    s_ns_ab = Slot_NS.SlotNSAutoBet(driver, get_img, member_window)
    s_mg_ab = Slot_MG.SlotMGAutoBet(driver, get_img, member_window)
    s_jili_ab = Slot_JILI.SlotJILIAutoBet(driver, get_img, member_window)
    s_pp_ab = Slot_PP.SlotPPAutoBet(driver, get_img, member_window)
    s_fc_ab = Slot_FC.SlotFCAutoBet(driver, get_img, member_window)

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_argument(self):
        """ 確認參數 """

        # init
        result = True

        # 判斷測試環境(prod or uat)
        if test_env != "prod" and test_env != "uat":
            print(f"Fail：「test_env」參數錯誤，參數值：{test_env}")
            print("目前可接受參數值為：")
            print("參數值：uat，取「測試站」設定")
            print("參數值：prod，取「正式站」設定")
            self.__class__.test_continue = False
            result = False

        # 判斷測試遊戲(全部 or RSG)
        elif test_game != "all" and test_game != "rsg":
            print(f"Fail：「test_game」參數錯誤，參數值：{test_game}")
            print("目前可接受參數值為：")
            print("參數值：rsg，只測試「RSG電子」")
            print("參數值：all，測試「所有電子」")
            self.__class__.test_continue = False
            result = False

        # 判斷測試RSG類別(全部 or 熱門)
        elif rsg_type != "all" and rsg_type != "hot":
            print(f"Fail：「rsg_type」參數錯誤，參數值：{rsg_type}")
            print("目前可接受參數值為：")
            print("參數值：hot，執行熱門遊戲")
            print("參數值：all，執行全部遊戲")
            self.__class__.test_continue = False
            result = False

        # 正式站目前只開放RSG
        elif test_env == "prod" and test_game == "all":
            print("正式站目前只開放 RSG")
            print(f"「test_game」請修改為：rsg")
            print(f"目前參數：{test_game}")
            self.__class__.test_continue = False
            result = False

        else:
            print("======= 參數正確 =======")
            print(f"測試環境：{test_env}")
            print(f"測試遊戲：{test_game}")
            print(f"測試RSG類別：{rsg_type}")

        self.assertEqual(True, result)

    def test_login(self):
        """ 登入 """

        # init
        result = True
        driver.get(test_data['member_url'])
        time.sleep(2)

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result = False

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result = False

        self.assertEqual(True, result)

    def test_slot_rsg(self):
        """ RSG電子自動下注 """

        result = self.s_rsg_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_jdb(self):
        """ JDB電子自動下注 """

        result = self.s_jdb_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_ds(self):
        """ DS電子自動下注 """

        result = self.s_ds_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_gemini(self):
        """ GEMINI電子自動下注 """

        result = self.s_gemini_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_joker(self):
        """ JOKER電子自動下注 """

        result = self.s_joker_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_ameba(self):
        """ Ameba電子自動下注 """

        result = self.s_ambea_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_tp(self):
        """ TP電子自動下注 """

        result = self.s_tp_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_gr(self):
        """ GR電子自動下注 """

        result = self.s_gr_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_ns(self):
        """ NS電子自動下注 """

        result = self.s_ns_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_mg(self):
        """ MG電子自動下注 """

        result = self.s_mg_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_jili(self):
        """ JILI電子自動下注 """

        result = self.s_jili_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_pp(self):
        """ PP電子自動下注 """

        result = self.s_pp_ab.auto_bet()
        self.assertEqual(True, result)

    def test_slot_fc(self):
        """ FC電子自動下注 """

        result = self.s_fc_ab.auto_bet()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        SlotBet("test_argument"),
        SlotBet("test_login")
    ])
    if test_game == 'all':
        test_units.addTests([
            SlotBet("test_slot_rsg"),
            SlotBet("test_slot_jdb"),
            SlotBet("test_slot_ds"),
            SlotBet("test_slot_gemini"),
            SlotBet("test_slot_joker"),
            SlotBet("test_slot_ameba"),
            SlotBet("test_slot_tp"),
            SlotBet("test_slot_gr"),
            SlotBet("test_slot_ns"),
            SlotBet("test_slot_mg"),
            SlotBet("test_slot_jili"),
            SlotBet("test_slot_pp"),
            SlotBet("test_slot_fc")
        ])
    elif test_game == 'rsg':
        test_units.addTests([
            SlotBet("test_slot_rsg"),
        ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/Bet/Bet_' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title=f'電子自動下注 - 測試環境: {test_env}，測試遊戲: {test_game}，測試 rsg 種類: {rsg_type}',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
