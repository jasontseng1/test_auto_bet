# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Lib import img_recognition
from Lib import get_report
from Lib.window_functions import WindowFunctions as wf

game_list_dict = {
    "神燈金靈": "GoldenGenie_bet.png",
    "財富連連": "LuckyFortunes_bet.png",
}


class SlotFCAutoBet:
    """ FC電子自動下注 """

    def __init__(self, driver, get_img, member_window):
        self.driver = driver
        self.get_img = get_img
        self.member_window = member_window

    def auto_bet(self):
        """ 自動下注 """

        result_list = []
        w_f = wf(self.driver, self.get_img)

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='nav']//a[./span[text()='電子']]"))).click()

        title = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='content']")))
        self.driver.execute_script("arguments[0].scrollIntoView();", title)

        # 點擊FC
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='FC']"))).click()

        # 神燈金靈
        try:
            print("測試項目：「神燈金靈」自動下注")
            # 點擊開啟神燈金靈
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='神燈金靈 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換至新分頁
            check_window = w_f.enter_to_new_window(self.member_window)
            if not check_window:
                result_list.append("False")
            else:
                # 判斷是否進入下注區
                img_coord = img_recognition.CoordLocator(self.driver, 'Slot', 'FC', game_list_dict["神燈金靈"])
                img_path = img_coord.check_img()
                click_result = img_coord.get_coordinate(target=img_path, canny=False)
                if not click_result:
                    result_list.append("False")
                else:
                    x, y = click_result

                    # 下注 (X軸座標-100)
                    ac = ActionChains(self.driver)
                    ac.reset_actions()
                    ac.pause(1).move_by_offset(x - 100, y).click().perform()
                    time.sleep(1)
                    print("***自動下注成功***")

                self.get_img("神燈金靈")
                time.sleep(1)

                # 返回官網
                w_f.back_to_window(self.member_window)

        except Exception as e:
            self.get_img("Fail:「神燈金靈」自動下注失敗")
            print(e)
            w_f.back_to_window(self.member_window)
            result_list.append("False")

        # 財富連連
        try:
            print("測試項目：「財富連連」自動下注")
            # 點擊開啟財富連連
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[text()='財富連連 ']"))).click()

            # wait loading
            WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
            time.sleep(1)

            # 切換至新分頁
            check_window = w_f.enter_to_new_window(self.member_window)
            if not check_window:
                result_list.append("False")
            else:
                # 判斷是否進入下注區
                img_coord = img_recognition.CoordLocator(self.driver, 'Slot', 'FC', game_list_dict["財富連連"])
                img_path = img_coord.check_img()
                click_result = img_coord.get_coordinate(target=img_path, canny=False)
                if not click_result:
                    result_list.append("False")
                else:
                    x, y = click_result

                    # 下注 (X軸座標-200)
                    ac = ActionChains(self.driver)
                    ac.reset_actions()
                    ac.pause(1).move_by_offset(x - 200, y).click().perform()
                    time.sleep(1)
                    print("***自動下注成功***")

                self.get_img("財富連連")
                time.sleep(1)

                # 返回官網
                w_f.back_to_window(self.member_window)

        except Exception as e:
            self.get_img("Fail:「財富連連」自動下注失敗")
            print(e)
            w_f.back_to_window(self.member_window)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class SlotFCAutoReport:
    """ FC電子自動報表 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []

        # 切換分頁-官網
        handles = self.driver.window_handles
        self.driver.switch_to.window(handles[0])

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', 'FC')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'FC')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-後台
        self.driver.switch_to.window(handles[1])

        # 取得後台報表資料
        get_manage_report = get_report.ManageReport(self.driver, self.get_img)
        manager_report, page_data, page_check_result = get_manage_report.search_report('3', 'FC', check_data)
        if not manager_report or not page_check_result:
            result_list.append("False")

        # 確認資料
        manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'FC')
        if manage_check_data:
            # 整理遊戲名稱
            manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

            if not manage_check_data['確認結果']:
                result_list.append("False")

        # 切換分頁-超帳
        self.driver.switch_to.window(handles[-1])

        # 取得超帳報表資料
        super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', 'FC', check_data)
        if not super_report or not super_page_check_result:
            result_list.append("False")

        # 確認資料
        super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'FC', True)
        if super_check_data:
            # 整理遊戲名稱
            super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

            if not super_check_data['確認結果']:
                result_list.append("False")

        # 比對官網與後台報表
        check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
        if not check_report_result:
            result_list.append("False")

        # 比對官網與超帳報表
        check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
        if not check_super_report_result:
            result_list.append("False")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], game_list_dict, '官網')
            if not check_member_game_result:
                result_list.append("False")

        # 比對後台報表遊戲清單
        if manage_check_data:
            check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], game_list_dict, '後台')
            if not check_manage_game_result:
                result_list.append("False")

        # 比對超帳報表遊戲清單
        if super_check_data:
            check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], game_list_dict, '超帳')
            if not check_super_game_result:
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
