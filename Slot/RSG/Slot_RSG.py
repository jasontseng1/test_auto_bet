# _*_ coding: UTF-8 _*_
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
from Setting import *
from Lib import img_recognition
from Lib import get_report
from Lib.get_api import Api
from Lib.window_functions import WindowFunctions as wf

game_list_dict = {
    "戰神呂布": "bet1.png",
    "聚寶財神": "bet2.png",
    "羅馬競技場": "bet1.png",
    "法老王": "bet1.png",
    "雷神之錘": "bet1.png",
    "麻將發了": ["start1.png", "bet2.png"],
    "龍王2": "bet1.png",
    "皇家777": "bet1.png",
    "魔龍傳奇": ["start2.png", "bet5.png"],
    "法老王 II": "bet1.png",
    "開心農場": "bet4.png",
    "幸運水果": "bet1.png",
    "五龍爭霸": "bet1.png",
    "七十二變": "bet1.png",
    "美狄亞": "bet1.png",
    "美杜莎": "bet1.png",
    "龍王": "bet1.png",
    "皇家7777": "bet1.png",
    "阿里巴巴": "bet1.png",
    "秦皇傳說": "bet1.png",
    "金鷄報喜": "bet1.png",
    "動物叢林": "bet1.png",
    "野蠻遊戲": "bet1.png",
    "大力水手": "bet1.png",
    "瘋狂博士": "bet1.png",
    "人魚傳說": "bet1.png",
    "魔法石": "bet1.png",
    "竹林熊貓": "bet1.png",
    "泰有錢": "bet1.png",
    "永不停止": "bet1.png",
    "慾望城市": "bet1.png",
    "財神到": "bet1.png",
    "行運一條龍": "bet1.png",
    "甜蜜糖果": "bet1.png",
    "泰好運": "bet1.png",
    "礦工哥布林": "bet1.png",
    "幸運拉霸": "bet1.png",
    "烈焰轉輪": "bet1.png",
    "荒野水牛": "bet1.png",
    "農場夜驚魂": "bet1.png",
    "家犬先生": "bet1.png",
    "HUSA": "bet1.png",
    "虎克船長": "bet1.png",
    "福娃發發": "bet1.png",
    "泰山": "bet1.png",
    "非洲": "bet1.png",
    "提金派對": "bet1.png",
    "金豬爆吉": "bet1.png",
    "武媚娘": "bet1.png",
    "跳跳獅": "bet1.png",
    "迪斯可之夜": "bet1.png",
    "果凍27": "bet1.png",
    "女忍者": "bet1.png",
    "忍者": "bet1.png",
    "巫師商店": "bet1.png",
    "霓虹圓": "bet1.png",
    "暴怒棕熊": "bet1.png",
    "嗨起來": "bet1.png",
    "墨西哥辣椒": "bet1.png",
    "秘林熊貓": "bet1.png",
    "龍行天下": "bet1.png",
    "能量外星人": "bet1.png",
    "七起來": "bet1.png",
    "小熊王國": "bet1.png",
    "賣火柴的小女孩": "bet1.png",
    "黃金之書": "bet1.png",
    "海神": "bet1.png",
    "西部牛仔": "bet1.png",
    "精靈射手": "bet1.png",
    "巫師商店黃金版": "bet1.png",
    "太極": "bet1.png",
    "嘻哈金剛": "bet1.png",
    "鼠來寶": "bet1.png",
    "潑水節": "bet1.png",
    "金色幸運草": "bet1.png",
}


class SlotRSGAutoBet:
    """ RSG電子自動下注 """

    def __init__(self, driver, get_img, test_env, rsg_type, member_window):
        self.driver = driver
        self.get_img = get_img
        self.test_env = test_env
        self.rsg_type = rsg_type
        self.member_window = member_window

    def select_game_tab(self, get_game_name=False):
        """
        選擇遊戲頁簽
        參數:
            get_game_name (bool): 取得遊戲名稱，預設為 False

        Returns:
            (list|bool): 點擊頁簽成功 or 回傳遊戲名稱清單，回傳 True|list，失敗則是 False
        """

        try:
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='nav']//a[./span[text()='電子']]"))).click()

            # 點擊RSG
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='RSG']"))).click()

            # 判斷種類
            if self.rsg_type == 'hot':
                # 點擊熱門頁簽
                ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()='熱門']")))
                ele.click()
                time.sleep(1)
                if 'active' not in ele.get_attribute('class'):
                    ele.click()
                    time.sleep(1)

            elif self.rsg_type == 'all':
                # 點擊全部頁簽
                ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[text()='全部']")))
                ele.click()
                time.sleep(1)
                if 'active' not in ele.get_attribute('class'):
                    ele.click()
                    time.sleep(1)

            if get_game_name:
                check_game_name_list = self.get_game_name()
                if check_game_name_list:
                    return check_game_name_list
                else:
                    return False

            return True
        except:
            print("Fail：點擊頁簽異常")
            return False

    def get_game_name(self):
        """ 取得遊戲名稱 """

        try:
            # init
            get_list = {}

            game_list = self.driver.find_elements(By.XPATH, "//div[@class='panel_gamelist']")
            for game in game_list:
                _name = game.find_element(By.XPATH, ".//div[@class='panel_name']")
                game_name = _name.text

                if game_name in game_list_dict.keys():
                    get_list[game_name] = game_list_dict[game_name]
                else:
                    print(f"{game_name} 不在全部清單中")

            return get_list
        except:
            print("Fail：取得遊戲名稱異常")
            return False

    def check_game_number(self, member_number, game_number):
        """
        確認遊戲數量
        參數:
            member_number (int): 官網遊戲數量
            game_number (int): 遊戲清單數量

        Returns:
            (bool): 數量相同，回傳 True，不同或是異常則是 False
        """

        result = True

        try:
            # 判斷遊戲數量
            if member_number != game_number:
                maintain_list = self.driver.find_elements(By.XPATH, "//div[@class='panel_gamelist maintain']")
                for i in maintain_list:
                    # 移動至元素，y-200 避免截圖時被上面選單擋住
                    self.driver.execute_script(f"window.scrollTo({i.location['x']}, {i.location['y'] - 200})")
                    game_name = i.find_element(By.XPATH, ".//div[@class='panel_name']").text
                    print(f"Fail：遊戲「{game_name}」顯示「維護中」")
                    self.get_img(f"{game_name}")
                    time.sleep(1)

                # 回到上方
                title = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@class='content']")))
                self.driver.execute_script("arguments[0].scrollIntoView();", title)

                result = False

            else:
                print("「官網遊戲」與「遊戲清單」數量相同\n")

        except Exception as e:
            print("Fail：判斷數量時異常")
            print(e)
            result = False

        return result

    def auto_bet(self):
        """ 自動下注 """

        # init
        game_name = None
        result_list = []
        w_f = wf(self.driver, self.get_img)

        try:
            # 確認 遊戲是否正常及開啟
            # RSG 的 h1 開關有兩個，「'皇家電子廠商開關', '皇家電子廠商開關'」
            if self.test_env == 'uat':
                # h1 需判斷兩個開關，因普遍為A線帳號
                for name in ['皇家電子開關', '皇家電子廠商開關']:
                    check_result = Api.check_w1_and_h1_data(test_data['api'], 'RSG', name)
                    if not check_result:
                        return False

            elif self.test_env == 'prod':
                # 正式站電測只需判斷 '皇家電子廠商開關' 及可，因電測帳號都是D線的，不受 '皇家電子開關' 影響
                check_result = Api.check_w1_and_h1_data(test_data['api'], 'RSG', '皇家電子廠商開關')
                if not check_result:
                    return False

            # 選擇頁簽
            check_tab = self.select_game_tab()
            if not check_tab:
                return False

            game_list = self.driver.find_elements(By.XPATH, "//div[@class='panel_gamelist']")

            # 判斷遊戲數量
            if self.rsg_type == 'all':
                result = self.check_game_number(len(game_list), len(game_list_dict))
                if not result:
                    result_list.append("False")

            for game in game_list:
                _name = game.find_element(By.XPATH, ".//div[@class='panel_name']")
                game_name = _name.text

                print(f"測試項目：「{game_name}」自動下注")
                if isinstance(game_list_dict[game_name], list):
                    img_coord = img_recognition.CoordLocator(self.driver, 'Slot', 'RSG', game_list_dict[game_name][0])
                else:
                    img_coord = img_recognition.CoordLocator(self.driver, 'Slot', 'RSG', game_list_dict[game_name])

                img_path = img_coord.check_img()

                # 判斷字典檔有無該遊戲 & 圖片路徑
                if game_name not in game_list_dict:
                    print(f"Fail: RSG 無 {game_name} 遊戲")
                    result_list.append("False")
                    continue
                elif not img_path:
                    result_list.append("False")
                    continue

                # 點擊名稱進遊戲
                _name.click()
                time.sleep(1)

                # wait loading
                WebDriverWait(self.driver, 10).until(EC.invisibility_of_element_located(
                    (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
                time.sleep(1)

                # 切換至新分頁
                check_window = w_f.enter_to_new_window(self.member_window)
                if not check_window:
                    result_list.append("False")
                    continue

                if game_name == '聚寶財神':
                    WebDriverWait(self.driver, 70).until(EC.element_to_be_clickable(
                        (By.XPATH, "//img[@class='getStartTxt']"))).click()

                # 判斷是否進入下注區
                if game_list_dict[game_name] == 'bet3.png':
                    click_result = img_coord.get_coordinate(target=img_path, canny=False, re_load=True)
                    if not click_result:
                        result_list.append("False")
                    else:
                        x, y = click_result

                        # 點擊按鈕進入遊戲 (X軸座標+40，Y軸座標+180)
                        ac = ActionChains(self.driver)
                        ac.reset_actions()
                        ac.pause(1).move_by_offset(x + 40, y + 180).click().perform()
                        time.sleep(1)
                else:
                    click_result = img_coord.click_coordinate(img_path=img_path, re_load=True)

                # 此兩款需要二次點擊
                if game_name == '麻將發了' or game_name == '魔龍傳奇':
                    img_coord = img_recognition.CoordLocator(self.driver, 'Slot', 'RSG', game_list_dict[game_name][1])
                    img_path = img_coord.check_img()
                    click_result = img_coord.click_coordinate(img_path=img_path, re_load=True)

                if not click_result:
                    result_list.append("False")
                else:
                    print("***自動下注成功***")

                self.get_img(f"{game_name}")
                time.sleep(1)

                # 返回官網
                w_f.back_to_window(self.member_window)
        except Exception as e:
            self.get_img(f"Fail:「{game_name}」自動下注失敗")
            print(e)
            w_f.back_to_window(self.member_window)
            result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results


class SlotRSGAutoReport:
    """ RSG電子自動報表 """

    def __init__(self, driver, get_img, test_env, rsg_type, window_data):
        self.driver = driver
        self.get_img = get_img
        self.test_env = test_env
        self.rsg_type = rsg_type
        self.window_data = window_data

    def auto_report(self):
        """ 自動報表 """

        # init
        result_list = []
        manager_report = False
        manage_check_data = False
        super_report = False
        super_check_data = False

        # 切換至官網
        self.driver.switch_to.window(self.window_data['member_window'])

        # 判斷測試類別(熱門or全部)
        if self.rsg_type == 'hot':
            check_game_list = SlotRSGAutoBet(self.driver, self.get_img, self.test_env, self.rsg_type,
                                             self.window_data['member_window']).select_game_tab(get_game_name=True)
            if not check_game_list:
                return False

        elif self.rsg_type == 'all':
            check_game_list = game_list_dict

        # 取得官網報表資料
        get_member_report = get_report.MemberReport(self.driver, self.get_img)
        member_report = get_member_report.search_report('3', 'RSG')
        if not member_report:
            result_list.append("False")

        # 確認資料
        check_data = get_member_report.check_web_and_report_data(member_report, 'RSG')
        if check_data:
            # 整理遊戲名稱
            check_data["遊戲"] = list(set([k['遊戲'] for i in member_report for j in i['種類'] for k in i['種類'][j]]))

            if not check_data['確認結果']:
                result_list.append("False")

        get_manage_report = get_report.ManageReport(self.driver, self.get_img)

        # 切換至代理端
        if self.window_data['manage_window'] is not None and self.test_env == 'uat':
            self.driver.switch_to.window(self.window_data['manage_window'])

            # 取得後台報表資料
            manager_report, page_data, page_check_result = get_manage_report.search_report('3', 'RSG', check_data)
            if not manager_report or not page_check_result:
                result_list.append("False")

            # 確認資料
            manage_check_data = get_manage_report.check_web_and_report_data(manager_report, page_data, 'RSG')
            if manage_check_data:
                # 整理遊戲名稱
                manage_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in manager_report for j in i['內容'] for k in i['內容'][j]]))

                if not manage_check_data['確認結果']:
                    result_list.append("False")

        # 切換至超帳
        if self.window_data['super_manage_window'] is not None and self.test_env == 'uat':
            self.driver.switch_to.window(self.window_data['super_manage_window'])

            # 取得超帳報表資料
            super_report, super_page_data, super_page_check_result = get_manage_report.super_search_report('3', 'RSG', check_data)
            if not super_report or not super_page_check_result:
                result_list.append("False")

            # 確認資料
            super_check_data = get_manage_report.check_web_and_report_data(super_report, super_page_data, 'RSG', True)
            if super_check_data:
                # 整理遊戲名稱
                super_check_data["遊戲"] = list(set([k['遊戲名稱'] for i in super_report for j in i['內容'] for k in i['內容'][j]]))

                if not super_check_data['確認結果']:
                    result_list.append("False")

        if self.test_env == 'uat':
            # 比對官網與後台報表
            check_report_result = get_report.check_member_report_and_manager_report(member_report, manager_report)
            if not check_report_result:
                result_list.append("False")

            # 比對官網與超帳報表
            check_super_report_result = get_report.check_member_report_and_manager_report(member_report, super_report, '超帳')
            if not check_super_report_result:
                result_list.append("False")

        if self.rsg_type == 'all':
            print("==================== 確認有無遊戲維護中 - 檢查開始 ====================\n")
            # 切換至官網
            self.driver.switch_to.window(self.window_data['member_window'])
            s_ab = SlotRSGAutoBet(self.driver, self.get_img, self.test_env, self.rsg_type,
                           self.window_data['member_window'])
            member_list = s_ab.select_game_tab(get_game_name=True)
            check_result = s_ab.check_game_number(len(member_list), len(game_list_dict))
            if not check_result:
                result_list.append("False")
            print("==================== 確認有無遊戲維護中 - 檢查結束 ====================\n")

        # 比對官網報表遊戲清單
        if check_data:
            check_member_game_result = get_report.check_game_list(check_data['遊戲'], check_game_list, '官網')
            if not check_member_game_result:
                result_list.append("False")

        if self.test_env == 'uat':
            # 比對後台報表遊戲清單
            if manage_check_data:
                check_manage_game_result = get_report.check_game_list(manage_check_data['遊戲'], check_game_list, '後台')
                if not check_manage_game_result:
                    result_list.append("False")

            # 比對超帳報表遊戲清單
            if super_check_data:
                check_super_game_result = get_report.check_game_list(super_check_data['遊戲'], check_game_list, '超帳')
                if not check_super_game_result:
                    result_list.append("False")

        results = False if "False" in str(result_list) else True
        return results
