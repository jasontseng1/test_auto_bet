# _*_ coding: UTF-8 _*_
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import time
import unittest
from Setting import *
from Lib import get_report
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from RSG import Slot_RSG
from JDB import Slot_JDB
from DS import Slot_DS
from GEMINI import Slot_GEMINI
from JOKER import Slot_JOKER
from Ameba import Slot_Ameba
from TP import Slot_TP
from GR import Slot_GR
from NS import Slot_NS
from MG import Slot_MG
from JILI import Slot_JILI
from PP import Slot_PP
from FC import Slot_FC

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()

# 視窗控制
window_dict = {
    'member_window': driver.current_window_handle,
    'manage_window': None,
    'super_manage_window': None
}


class SlotReport(unittest.TestCase):
    """ 電子報表 """

    s_rsg_ar = Slot_RSG.SlotRSGAutoReport(driver, get_img, test_env, rsg_type, window_dict)
    s_jdb_ar = Slot_JDB.SlotJDBAutoReport(driver, get_img)
    s_ds_ar = Slot_DS.SlotDSAutoReport(driver, get_img)
    s_gemini_ar = Slot_GEMINI.SlotGEMINIAutoReport(driver, get_img)
    s_joker_ar = Slot_JOKER.SlotJOKERAutoReport(driver, get_img)
    s_ambea_ar = Slot_Ameba.SlotAmebaAutoReport(driver, get_img)
    s_tp_ar = Slot_TP.SlotTPAutoReport(driver, get_img)
    s_gr_ar = Slot_GR.SlotGRAutoReport(driver, get_img)
    s_ns_ar = Slot_NS.SlotNSAutoReport(driver, get_img)
    s_mg_ar = Slot_MG.SlotMGAutoReport(driver, get_img)
    s_jili_ar = Slot_JILI.SlotJILIAutoReport(driver, get_img)
    s_pp_ar = Slot_PP.SlotPPAutoReport(driver, get_img)
    s_fc_ar = Slot_FC.SlotFCAutoReport(driver, get_img)

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_argument(self):
        """ 確認參數 """

        result = True

        # 判斷測試環境(prod or uat)
        if test_env != "prod" and test_env != "uat":
            print(f"Fail：「test_env」參數錯誤，參數值：{test_env}")
            print("目前可接受參數值為：")
            print("參數值：uat，取「測試站」設定")
            print("參數值：prod，取「正式站」設定")
            self.__class__.test_continue = False
            result = False

        # 判斷測試遊戲(全部 or RSG)
        elif test_game != "all" and test_game != "rsg":
            print(f"Fail：「test_game」參數錯誤，參數值：{test_game}")
            print("目前可接受參數值為：")
            print("參數值：rsg，只測試「RSG電子」")
            print("參數值：all，測試「所有電子」")
            self.__class__.test_continue = False
            result = False

        # 判斷測試RSG類別(全部 or 熱門)
        elif rsg_type != "all" and rsg_type != "hot":
            print(f"Fail：「rsg_type」參數錯誤，參數值：{rsg_type}")
            print("目前可接受參數值為：")
            print("參數值：hot，執行熱門遊戲")
            print("參數值：all，執行全部遊戲")
            self.__class__.test_continue = False
            result = False

        # 正式站目前只開放RSG
        elif test_env == "prod" and test_game == "all":
            print("正式站目前只開放 RSG")
            print(f"「test_game」請修改為：rsg")
            print(f"目前參數：{test_game}")
            self.__class__.test_continue = False
            result = False

        else:
            print("======= 參數正確 =======")
            print(f"測試環境：{test_env}")
            print(f"測試遊戲：{test_game}")
            print(f"測試RSG類別：{rsg_type}")

        self.assertEqual(True, result)

    def test_login(self):
        """ 登入 """

        # init
        result_list = []
        driver.get(test_data['member_url'])
        time.sleep(2)

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result_list.append("False")
            else:
                window_dict['member_window'] = driver.current_window_handle

            get_img("登入成功")
        except:
            get_img("登入失敗")
            result_list.append("False")

        if test_env == 'uat':
            # 設定視窗資訊
            try:
                get_manage_report = get_report.ManageReport(driver, get_img)
                # 登入代理端
                manage_check = get_manage_report.login()
                if manage_check:
                    window_dict['manage_window'] = driver.current_window_handle

                # 登入超帳
                super_manage_check = get_manage_report.super_login()
                if super_manage_check:
                    window_dict['super_manage_window'] = driver.current_window_handle

            except:
                if window_dict['manage_window'] is not None:
                    print("Fail：登入「代理」異常")
                elif window_dict['super_manage_window'] is not None:
                    print("Fail：登入「超帳」異常")
                result_list.append("False")

        results = False if "False" in str(result_list) else True
        self.assertEqual(True, results)

    def test_slot_rsg_report(self):
        """ RSG電子報表確認 """

        result = self.s_rsg_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_jdb_report(self):
        """ JDB電子報表確認 """

        result = self.s_jdb_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_ds_report(self):
        """ DS電子報表確認 """

        result = self.s_ds_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_gemini_report(self):
        """ GEMINI電子報表確認 """

        result = self.s_gemini_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_joker_report(self):
        """ JOKER電子報表確認 """

        result = self.s_joker_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_ameba_report(self):
        """ Ameba電子報表確認 """

        result = self.s_ambea_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_tp_report(self):
        """ TP電子報表確認 """

        result = self.s_tp_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_gr_report(self):
        """ GR電子報表確認 """

        result = self.s_gr_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_ns_report(self):
        """ NS電子報表確認 """

        result = self.s_ns_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_mg_report(self):
        """ MG電子報表確認 """

        result = self.s_mg_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_jili_report(self):
        """ JILI電子報表確認 """

        result = self.s_jili_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_pp_report(self):
        """ PP電子報表確認 """

        result = self.s_pp_ar.auto_report()
        self.assertEqual(True, result)

    def test_slot_fc_report(self):
        """ FC電子報表確認 """

        result = self.s_fc_ar.auto_report()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        SlotReport("test_argument"),
        SlotReport("test_login")
    ])
    if test_game == 'all':
        test_units.addTests([
            SlotReport("test_slot_rsg_report"),
            SlotReport("test_slot_jdb_report"),
            SlotReport("test_slot_ds_report"),
            SlotReport("test_slot_gemini_report"),
            SlotReport("test_slot_joker_report"),
            SlotReport("test_slot_ameba_report"),
            SlotReport("test_slot_tp_report"),
            SlotReport("test_slot_gr_report"),
            SlotReport("test_slot_ns_report"),
            SlotReport("test_slot_mg_report"),
            SlotReport("test_slot_jili_report"),
            SlotReport("test_slot_pp_report"),
            SlotReport("test_slot_fc_report"),
        ])
    elif test_game == 'rsg':
        test_units.addTests([
            SlotReport("test_slot_rsg_report"),
        ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/Report/Report_' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title=f'電子自動報表 - 測試環境: {test_env}，測試遊戲: {test_game}，測試 rsg 種類: {rsg_type}',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
